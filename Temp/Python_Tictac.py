import sys
import random

from colorama import Fore
from colorama import Style

x = f'{Fore.GREEN}X{Style.RESET_ALL}'
o = f'{Fore.RED}O{Style.RESET_ALL}'

index = [str(num) for num in range(0, 10)]


def show_game():
    print(f' {index[1]} | {index[2]} | {index[3]} ')
    print('-----------')
    print(f' {index[4]} | {index[5]} | {index[6]}')
    print('-----------')
    print(f' {index[7]} | {index[8]} | {index[9]}')
    print('\n')


def judgementGame(who):
    res = False
    res |= (len({index[1], index[2], index[3]}) == 1)
    res |= (len({index[4], index[5], index[6]}) == 1)
    res |= (len({index[7], index[8], index[9]}) == 1)
    res |= (len({index[1], index[4], index[7]}) == 1)
    res |= (len({index[2], index[5], index[8]}) == 1)
    res |= (len({index[3], index[6], index[9]}) == 1)
    res |= (len({index[1], index[5], index[9]}) == 1)
    res |= (len({index[3], index[5], index[7]}) == 1)
    if res:
        print(f'{who} win the game.')
        sys.exit(0)


show_game()

while True:
    user_select = input('X, in what index in board.\n')
    try:
        user_choose = int(user_select)
        if not (0 < user_choose < 10):
            print('Invalid input, please choose the number in board.\n')
            continue
        if index[user_choose] in [x, o]:
            print('Invalid input, please choose the number in board.\n')
            continue
    except:
        print('Invalid input, please choose the number in board.\n')
        continue

    index[user_choose] = x
    show_game()
    judgementGame('User')
    while True:
        random_select = random.randint(1, 9)
        if index[random_select] in [x, o]:
            continue
        index[random_select] = o
        break
    show_game()
    judgementGame('PC')
