# 1379. Find a Corresponding Node of a Binary Tree in a Clone of That Tree
# https://leetcode.com/problems/find-a-corresponding-node-of-a-binary-tree-in-a-clone-of-that-tree/

# Example:
import os.path
import sys

example_nums = [3, 2, 4]
example_target = 6

# def two_sum1(nums: list[int], target: int) -> list[int]:
#     return 0


if __name__ == '__main__':
    question = r'2477. Minimum Fuel Cost to Report to the Capital'
    url = r'https://leetcode.com/problems/minimum-fuel-cost-to-report-to-the-capital/description/'
    function = r'def minimumFuelCost(self, roads: List[List[int]], seats: int) -> int:'
    function_name = ''
    check = True
    param = ''
    for parameter in function.split(',')[1:]:
        param += 'example_'
        parameter = parameter.strip()
        for char in parameter:
            if char == ':':
                break
            param += char
        param += ', '
    param = param[:-2]
    for index, val in enumerate(list(function)):
        if check and val.islower():
            function_name += val.lower()
            continue
        if check and val.isupper():
            function_name += '_' + val.lower()
            continue
        if val == '(':
            check = False
        function_name += val.lower()
    question_file = question.strip().title().replace('.', '_').replace(' ', '')
    function_name = function_name.strip().replace('list', 'List').replace('optional', 'Optional')\
        .replace('treeNode', 'TreeNode').replace('(', '1(')
    with open('0.py', 'r') as file_handler:
        content = file_handler.readlines()
    if os.path.exists('LeetCode_' + question_file + '.py'):
        sys.exit(1)
    with open('LeetCode_' + question_file + '.py', mode='w') as fp:
        for line in content:
            if '# Head' in line:
                line = '# {}'.format(question.strip())
                fp.write(line + '\n')
            elif '# URL' in line:
                line = '# {}'.format(url.strip())
                fp.write(line + '\n')
            elif '# Example' in line:
                line = '# Example'
                fp.write(line + '\n')
                for item in param.split(','):
                    line = '{} = None'.format(item.strip())
                    fp.write(line + '\n')
            elif '# def' in line:
                line = 'class Solution:'
                fp.write(line + '\n')
                line = '    {}'.format(function_name.strip())
                fp.write(line + '\n')
            elif '# func' in line:
                line = "if __name__ == '__main__':"
                fp.write(line + '\n')
                line = '    sol = Solution()'
                fp.write(line + '\n')
                line = '    print(sol.{}({}))'.format(
                    function_name.split(' ')[1].split('(')[0], param)
                fp.write(line + '\n')
            else:
                fp.write(line)
        fp.close()
    if os.path.exists(question_file + '.py'):
        sys.exit(0)
    else:
        sys.exit(1)
