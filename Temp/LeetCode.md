# Algorithms:
- Sorting, searching, and binary search
- Divide-and-conquer
- Dynamic programming and memorization
- Greedy algorithms
- Recursion
- Graph traversal, breadth-and depth-first
# Data structures you should know:
- Arrays
- Maps and Hash table
- Dynamic Programming
- Linked lists
- Stacks and Queues
- Heap
- Binary Trees
- Heaps
- Graphs 
- Greedy
- Topological sorting

