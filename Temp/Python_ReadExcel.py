import openpyxl

if __name__ == '__main__':
    # 要讀取的黨名，跟程式放在一起
    file_name = 'area.xlsx'

    # 讀取Excel檔案
    workbook = openpyxl.load_workbook(file_name)
    print(workbook)

    # 取出第一頁
    sheet_1 = workbook.worksheets[0]
    # 取出第二頁
    sheet_2 = workbook.worksheets[1]
    print(sheet_1)
    print(sheet_2)
    # 頁簽
    print(sheet_1.title)
    print(sheet_2.title)
    # 頁row數量
    print(sheet_1.max_row)
    print(sheet_2.max_row)
    # 頁column數量
    print(sheet_1.max_column)
    print(sheet_2.max_column)

    # 把原來的A存起來
    collect_original_page1 = []
    for row_idx in range(1, sheet_1.max_row + 1):
        collect_original_page1.append(sheet_1.cell(row=row_idx, column=1).value)

    # 把原來的B存起來
    collect_original_page2 = []
    for row_idx in range(1, sheet_2.max_row + 1):
        collect_original_page2.append(sheet_2.cell(row=row_idx, column=1).value)
    print(f'{collect_original_page1=}')
    print(f'{collect_original_page2=}')

    # 判斷A 有沒有在B
    for row_idx in range(1, sheet_1.max_row + 1):
        if sheet_1.cell(row=row_idx, column=1).value not in collect_original_page2:
            sheet_1.cell(row=row_idx, column=2).value = 'Check A'
            print(sheet_1.cell(row=row_idx, column=1).value)
            print('Not found in B')

    # 判斷B 有沒有在A
    for row_idx in range(1, sheet_2.max_row + 1):
        if sheet_2.cell(row=row_idx, column=1).value not in collect_original_page1:
            sheet_2.cell(row=row_idx, column=2).value = 'Check B'
            print(sheet_2.cell(row=row_idx, column=1).value)
            print('Not found in A')

    workbook.save('area_m.xlsx')