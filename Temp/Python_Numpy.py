import numpy as np

array_a = np.array([[1, 2, 3], [1, 2, 3], [1, 2, 3]])
print(f'{array_a.shape=}')
print(f'{array_a.size=}')
print(f'{array_a=}')

array_b = array_a.T
print(f'{array_b.shape=}')
print(f'{array_b.size=}')
print(f'{array_b=}')
print(f'{array_b[0]=}')
print(f'{array_b[:,2]=}')

array_a = np.array([[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12], [13, 14, 15, 16]])
print(f'{array_a.shape=}')
print(f'{array_a.size=}')
print(f'{np.size(array_a,0)=}')
print(f'{np.size(array_a,1)=}')
print(f'{array_a=}')

array_b = array_a.reshape(2, 8)
print(f'{array_b.shape=}')
print(f'{array_b.size=}')
print(f'{array_b=}')
print(f'{array_b[0]=}')
print(f'{array_b[:,2]=}')

row_a = [1, 2, 3, 4, 5]
row_b = [6, 7, 8, 9, 10]
row_c = [11, 12, 13, 14, 15]
row_d = [16, 17, 18, 19, 20]
row_e = [21, 22, 23, 24, 25]

array_a = np.array([row_a, row_b, row_c, row_d, row_e])
print(f'{array_a=}')
print(f'{array_a[:,2:4:1]=}')  # start: ending value :step size

# boolean
greater_than_5 = array_a > 5
print(f'{greater_than_5=}')
print(f'{array_a[greater_than_5]=}')

# logical_and
drop_under_5_and_over_20 = np.logical_and(array_a > 5, array_a < 20)
print(f'{drop_under_5_and_over_20=}')
print(f'{array_a[drop_under_5_and_over_20]=}')

print( (3 - 2 * 2 **0.5)**0.5 )
print( 2 **0.5 - 1 )