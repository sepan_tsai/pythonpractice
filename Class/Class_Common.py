# get_combinations, get_permutations => https://github.com/dokelung/Python-QA/tree/master/questions/string


class Common:
    @staticmethod
    def get_combinations(s: str):
        combs = []
        for i in range(1, 2 ** len(s)):
            pat = "{0:b}".format(i).zfill(len(s))
            combs.append(''.join(c for c, b in zip(s, pat) if int(b)))
        return list(set(combs))

    def get_permutations(self, char_list: list[str]):
        if len(char_list) == 1:
            return [char_list[0]]
        results = []
        for idx, c in enumerate(char_list):
            results += [c + substr for substr in self.get_permutations(char_list[:idx] + char_list[idx + 1:])]
        return results

    def backtracking(self, s, res=''):
        for idx, char in enumerate(s):
            res += char
            print(res)
            self.backtracking(s[idx+1:], res)
            res = res[:-1]

if __name__ == '__main__':
    com = Common()
    com.backtracking('abc')