import string


class MinHeap:
    def __init__(self, array):
        self.heap = self.build_heap(array)

    def build_heap(self, array):
        length = len(array)
        first_parent = (length - 2) // 2
        for idx in reversed(range(0, first_parent + 1)):
            self.sift_down(idx, length - 1, array)
        return array

    def sift_down(self, cur_idx, end_idx, heap):
        child_one_idx = cur_idx * 2 + 1
        while child_one_idx <= end_idx:
            child_two_idx = child_one_idx + 1 if (child_one_idx + 1) <= end_idx else -1
            if child_two_idx != -1 and heap[child_two_idx] < heap[child_one_idx]:
                idx_to_swap = child_two_idx
            else:
                idx_to_swap = child_one_idx
            if heap[idx_to_swap] < heap[cur_idx]:
                self.swap(cur_idx, idx_to_swap, heap)
                cur_idx = idx_to_swap
                child_one_idx = cur_idx * 2 + 1
            else:
                return None

    def sift_up(self, cur_idx, heap):
        par_idx = (cur_idx - 1) // 2
        while cur_idx > 0 and heap[cur_idx] < heap[par_idx]:
            self.swap(cur_idx, par_idx, heap)
            cur_idx = par_idx
            par_idx = (cur_idx - 1) // 2
        return None

    def peek(self):
        return self.heap[0]

    def remove(self):
        self.swap(0, len(self.heap) - 1, self.heap)
        value_to_remove = self.heap.pop()
        self.siftDown(0, len(self.heap) - 1, self.heap)
        return value_to_remove

    def insert(self, value):
        self.heap.append(value)
        self.siftUp(len(self.heap) - 1, self.heap)

    @staticmethod
    def swap(i, j, heap):
        heap[i], heap[j] = heap[j], heap[i]
