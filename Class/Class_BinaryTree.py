# BinaryTree


class TreeNode:
    def __init__(self, data, left=None, right=None, level=0):
        self.data = data
        self.left = left
        self.right = right
        self.level = level


class BinaryTree:
    def __init__(self):
        self.root = None
        self.tree_list = list()

    def build(self, list_data: list[int]):
        list_data.sort()
        self.root = self.list_to_standard_binary_tree(list_data)

    def list_to_standard_binary_tree(self, list_data: list[int], level=0):
        if not list_data:
            return None
        mid_num = len(list_data) // 2
        node = TreeNode(list_data[mid_num])
        node.level = level
        node.left = self.list_to_standard_binary_tree(list_data[:mid_num], node.level + 1)
        node.right = self.list_to_standard_binary_tree(list_data[mid_num + 1:], node.level + 1)
        return node

    def insert(self, data):
        def _insert(_data, cur_node):
            if _data < cur_node.data:
                if not cur_node.left:
                    cur_node.left = TreeNode(data=_data)
                else:
                    _insert(data, cur_node.left)
            elif _data > cur_node.data:
                if not cur_node.right:
                    cur_node.right = TreeNode(data=_data)
                else:
                    _insert(data, cur_node.right)
            else:
                print("This val has existed")

        if self.root is None:
            self.root = TreeNode(data=data)
        else:
            _insert(data, self.root)

    def remove(self, data):
        queue = [self.root]
        node = None
        position = None
        if self.root.data == data:
            position = 'first'
        while queue:
            if position is not None:
                break
            node = queue.pop(0)
            if node.left is not None:
                if node.left.data == data:
                    position = 'left'
                queue.append(node.left)
            if node.right is not None:
                if node.right.data == data:
                    position = 'right'
                queue.append(node.right)
        if position is not None:
            if position == 'first':
                list_data = self.level_order(self.root)
                list_data.remove(data)
                self.root = self.list_to_standard_binary_tree(list_data=list_data)
            if position == 'left':
                list_data = self.level_order(node.left)
                list_data.remove(data)
                node.left = self.list_to_standard_binary_tree(list_data=list_data, level=node.left.level)
            if position == 'right':
                list_data = self.level_order(node.right)
                list_data.remove(data)
                node.right = self.list_to_standard_binary_tree(list_data=list_data, level=node.right.level)

    def pre_order(self, node=None):
        if node == self.root:
            self.tree_list = []
        if node is not None:
            self.tree_list.append(node.data)
            self.pre_order(node.left)
            self.pre_order(node.right)

    def in_order(self, node=None):
        if node == self.root:
            self.tree_list = []
        if node is not None:
            self.in_order(node.left)
            self.tree_list.append(node.data)
            self.in_order(node.right)

    def post_order(self, node=None):
        if node == self.root:
            self.tree_list = []
        if node is not None:
            self.post_order(node.left)
            self.post_order(node.right)
            self.tree_list.append(node.data)

    def level_order(self, node=None, print_none=True):
        self.tree_list = []
        queue = [node]
        while queue:
            node = queue.pop(0)
            if node is None:
                if print_none:
                    self.tree_list.append(None)
                continue
            self.tree_list.append(node.data)
            queue.append(node.left)
            queue.append(node.right)
        while self.tree_list[-1] is None:
            self.tree_list.pop(-1)
        return self.tree_list

    def print_tree(self, node=None):
        def _print_tree(_node):
            if _node:
                if _node.left:
                    node_left_data = _node.left.data
                else:
                    node_left_data = ''
                if _node.right:
                    node_right_data = _node.right.data
                else:
                    node_right_data = ''
                print('Level: {:5s} Node: {:8s} Left: {:8s} Right: {:8s}'
                      .format(str(_node.level), str(_node.data), str(node_left_data), str(node_right_data)))
                _print_tree(_node.left)
                _print_tree(_node.right)

        if node is None:
            node = self.root
        if node:
            print('{} Tree {}'.format('=' * 24, '=' * 24))
            _print_tree(node)

    def out_put_tree_list(self, method=None, node=None):
        if node is None:
            node = self.root
            self.print_tree(node)
        if method is None:
            print('{} List {}'.format('=' * 24, '=' * 24))
            self.pre_order(node)
            print('Pre Order   :{}'.format(self.tree_list))
            self.in_order(node)
            print('In Order    :{}'.format(self.tree_list))
            self.post_order(node)
            print('Post Order  :{}'.format(self.tree_list))
            self.level_order(node)
            print('Level Order :{}'.format(self.tree_list))
