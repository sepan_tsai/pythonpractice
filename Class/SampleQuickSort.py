class QuickSort:
    def quick_sort(self, array, startIdx, endIdx):
        if startIdx >= endIdx:
            return None

        leftIdx = startIdx
        rightIdx = endIdx

        while leftIdx != rightIdx:
            while array[rightIdx] > array[startIdx] and leftIdx < rightIdx:
                rightIdx -= 1
            while array[leftIdx] <= array[startIdx] and leftIdx < rightIdx:
                leftIdx += 1
            if leftIdx < rightIdx:
                self.swap(leftIdx, rightIdx, array)

        self.swap(startIdx, leftIdx, array)
        self.quick_sort(array, startIdx, rightIdx - 1)
        self.quick_sort(array, rightIdx + 1, endIdx)

    @staticmethod
    def swap(i, j, heap):
        heap[i], heap[j] = heap[j], heap[i]
