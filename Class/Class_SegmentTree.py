# SegmentTree
import string


class SegmentTree:
    def __init__(self, nums):
        self.n = len(nums)
        self.tree = [0] * (4 * self.n)  # 大小通常设置为数组长度的4倍
        self.build_tree(nums, 0, 0, self.n - 1)

    def build_tree(self, nums, tree_index, lo, hi):
        if lo == hi:
            self.tree[tree_index] = nums[lo]
            return
        mid = (lo + hi) // 2
        left_child = 2 * tree_index + 1
        right_child = 2 * tree_index + 2
        self.build_tree(nums, left_child, lo, mid)
        self.build_tree(nums, right_child, mid + 1, hi)
        self.tree[tree_index] = self.tree[left_child] + self.tree[right_child]

    def update(self, index, val):
        self.update_tree(0, 0, self.n - 1, index, val)

    def update_tree(self, tree_index, lo, hi, arr_index, val):
        if lo == hi:
            self.tree[tree_index] = val
            return
        mid = (lo + hi) // 2
        left_child = 2 * tree_index + 1
        right_child = 2 * tree_index + 2
        if arr_index > mid:
            self.update_tree(right_child, mid + 1, hi, arr_index, val)
        else:
            self.update_tree(left_child, lo, mid, arr_index, val)
        self.tree[tree_index] = self.tree[left_child] + self.tree[right_child]

    def sum_range(self, i, j):
        return self.query_tree(0, 0, self.n - 1, i, j)

    def query_tree(self, tree_index, lo, hi, i, j):
        if lo > j or hi < i:
            return 0
        if i <= lo and j >= hi:
            print(self.tree[tree_index], (i, j, lo, hi, ))
            return self.tree[tree_index]
        mid = (lo + hi) // 2
        left_child = 2 * tree_index + 1
        right_child = 2 * tree_index + 2
        left_sum = self.query_tree(left_child, lo, mid, i, j)
        right_sum = self.query_tree(right_child, mid + 1, hi, i, j)
        print(left_sum, right_sum)
        return left_sum + right_sum

string.ascii_letters
# 使用示例
nums = [1, 3, 5, 7, 9, 11]
st = SegmentTree(nums)
print(st.tree)

print("Sum of values in the range [1, 3]:", st.sum_range(1, 3))  # 输出区间[1, 3]的和
st.update(1, 10)  # 将索引1处的元素更新为10
print("Updated sum of values in the range [1, 3]:", st.sum_range(1, 3))  # 再次输出区间[1, 3]的和
