# BinarySearch


class BinarySearch:

    def __init__(self, num_list, target):
        self.num_list = num_list
        self.target = target

    def find_first_target(self):
        # value: [1, 1, 1, 2, 2, 2, 2, 3, 3]
        #        [X, X, X, O, O, O, O, O, O]
        left = 0
        right = len(self.num_list) - 1

        while left < right:
            mid = (left + right) // 2
            if self.num_list[mid] >= self.target:
                right = mid
            else:
                left = mid + 1
        if left >= len(self.num_list) or self.num_list[left] != self.target:
            return -1
        return left

    def find_last_target(self):
        # value: [1, 1, 1, 2, 2, 2, 2, 3, 3]
        #        [O, O, O, O, O, O, O, X, X]
        left = 0
        right = len(self.num_list)

        while left < right:
            mid = (left + right) // 2
            if self.num_list[mid] > self.target:
                right = mid - 1
            else:
                left = mid + 1
        if left >= len(self.num_list) or self.num_list[left] != self.target:
            return -1
        return left

    def find_first_higher_than_target(self):
        # value: [1, 1, 1, 2, 2, 2, 2, 3, 3]
        #        [X, X, X, X, X, X, X, O, O]
        if len(self.num_list) == 0:
            return -1
        left = 0
        right = len(self.num_list) - 1

        while left < right:
            mid = (left + right) // 2
            if self.num_list[mid] > self.target:
                right = mid
            else:
                left = mid + 1

        return left

    def find_last_smaller_than_target(self):
        # value: [1, 1, 1, 2, 2, 2, 2, 3, 3]
        #        [O, O, O, X, X, X, X, X, X]
        if len(self.num_list) == 0:
            return -1
        left = 0
        right = len(self.num_list) - 1

        while left + 1 < right:
            mid = (left + right) // 2
            if self.num_list[mid] >= self.target:
                right = mid - 1
            else:
                left = mid
        print(locals(), globals())
        return left


if __name__ == '__main__':
    sol = BinarySearch(num_list=[1, 1, 1, 2, 2, 2, 2, 4, 4], target=2)
    print(sol.find_first_target())
    print(sol.find_last_target())
    print(sol.find_first_higher_than_target())
    print(sol.find_last_smaller_than_target())
