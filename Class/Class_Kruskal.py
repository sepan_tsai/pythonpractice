# Kruskal

from collections import defaultdict


class Kruskal:

    def __init__(self, vertices):
        self.V = vertices  # node's count
        self.graph = []

    def addEdge(self, u, v, w):
        self.graph.append([u, v, w])

    # find node's root
    def find(self, parent, i):
        if parent[i] == i:
            return i
        return self.find(parent, parent[i])

    # union node
    def union(self, parent, rank, x, y):
        x_root = self.find(parent, x)
        y_root = self.find(parent, y)
        print(f'{x_root=}, {y_root=}')

        # lead to root
        parent[x] = x_root
        parent[y] = y_root

        # union two node tree
        print(f'{parent=}, {rank=}')
        if rank[x_root] < rank[y_root]:
            parent[x_root] = y_root
        elif rank[x_root] > rank[y_root]:
            parent[y_root] = x_root
        else:
            parent[y_root] = x_root
            rank[x_root] += 1
        print(f'{parent=}, {rank=}')

    def KruskalMST(self):

        result = []

        i = 0
        end = 0

        self.graph = sorted(self.graph, key=lambda item: item[2])
        print(f'{self.graph=}')
        parent = []
        rank = []

        for node in range(self.V):
            parent.append(node)
            rank.append(0)

        # V nodes need V - 1 side
        while end < self.V - 1:

            u, v, w = self.graph[i]
            x = self.find(parent, u)
            y = self.find(parent, v)
            print(f'{self.graph[i]=}, {parent=}, {rank=}')
            print(f'{x=}, {y=}')

            # if not same root, mean no ring
            if x != y:
                end += 1
                result.append([u, v, w])
                self.union(parent, rank, x, y)

            i += 1
        print(f'{self.graph[i]=}, {parent=}, {rank=}')
        print(f'{result=}')

        print("Following are the edges in the constructed MST")
        for u, v, weight in result:
            print("%d -- %d == %d" % (u, v, weight))


g = Kruskal(4)
g.addEdge(0, 1, 10)
g.addEdge(0, 2, 6)
g.addEdge(0, 3, 5)
g.addEdge(1, 3, 15)
g.addEdge(2, 3, 4)
g.KruskalMST()
