class UnionFind:
    def __init__(self, size):
        self.root = [i for i in range(size)]

    def find(self, x):
        if x == self.root[x]:
            return x
        self.root[x] = self.find(self.root[x])  # 路径压缩
        return self.root[x]

    def union(self, x, y):
        root_x = self.find(x)
        root_y = self.find(y)
        if root_x != root_y:
            self.root[root_y] = root_x
        print(self.root)

    def connected(self, x, y):
        return self.find(x) == self.find(y)


# 示例用法
edges = [[0, 1], [1, 2], [2, 3], [1, 3]]
uf = UnionFind(4)

for edge in edges:
    if uf.connected(edge[0], edge[1]):
        print("Found a cycle:", edge)
        break
    uf.union(edge[0], edge[1])
