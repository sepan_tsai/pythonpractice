class HeapSort:
    def heap_sort(self, array):
        self.build_max_heap(array)
        for endIdx in reversed(range(1, len(array))):
            self.swap(0, endIdx, array)
            self.sift_down(0, endIdx - 1, array)
        return array

    def build_max_heap(self, array):
        firstParentIdx = (len(array) - 2) // 2
        for curIdx in reversed(range(firstParentIdx + 1)):
            self.sift_down(curIdx, len(array) - 1, array)

    def sift_down(self, curIdx, endIdx, heap):
        childOneIdx = curIdx * 2 + 1
        while childOneIdx <= endIdx:
            childTwoIdx = curIdx * 2 + 2 if curIdx * 2 + 2 <= endIdx else -1
            if childTwoIdx > -1 and heap[childTwoIdx] > heap[childOneIdx]:
                idxToSwap = childTwoIdx
            else:
                idxToSwap = childOneIdx
            if heap[idxToSwap] > heap[curIdx]:
                self.swap(curIdx, idxToSwap, heap)
                curIdx = idxToSwap
                childOneIdx = curIdx * 2 + 1
            else:
                return None

    @staticmethod
    def swap(i, j, heap):
        heap[i], heap[j] = heap[j], heap[i]
