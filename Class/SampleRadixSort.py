class RadixSort:
    def radix_sort(self, array):
        if len(array) == 0:
            return array

        maxNums = max(array)
        maxDigit = 1
        while maxNums > (10 ** (maxDigit - 1)):
            maxDigit += 1

        digit = 1
        while digit <= maxDigit:
            buckets = [[] for _ in range(0, 10)]
            for num in array:
                buckets[self.getDigitNumber(num, digit)].append(num)
            array = self.bucketToArray(buckets)
            digit += 1

        return array

    @staticmethod
    def getDigitNumber(num, digit):
        return num // (10 ** (digit - 1)) % 10

    @staticmethod
    def bucketToArray(buckets):
        array = []
        for bucket in buckets:
            array = array + bucket
        return array
