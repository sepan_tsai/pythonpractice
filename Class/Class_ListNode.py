# ListNode


class ListNode:
    def __init__(self, data=0, next_node=None):
        self.data = data
        self.next = next_node


class SingleLinkedList:
    def __init__(self):
        self.head = None
        self.tail = None
        self.node_list = list()
        return

    def build(self, list_data: list[int]):
        for data in list_data:
            self.add_list_item(data)

    def add_list_item(self, node):
        if not isinstance(node, ListNode):
            node = ListNode(node)
        if self.head is None:
            self.head = node
        else:
            self.tail.next = node
        self.tail = node

    def print_list(self, node=None):
        def _print_tree(_node):
            if _node:
                self.node_list.append(_node.data)
                _print_tree(_node.next)
        if node is None:
            node = self.head
        _print_tree(node)

    def out_put_node_list(self, node=None):
        if node is None:
            node = self.head
            self.print_list(node)
        print('{} List {}'.format('=' * 24, '=' * 24))
        print('Node List : {}'.format(self.node_list))
        return self.node_list
