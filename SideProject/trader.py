import datetime
from dateutil.relativedelta import relativedelta
from tabulate import tabulate

import backtrader as bt
import yfinance as yf
from matplotlib import rcParams


class BollingerBands(bt.Strategy):
    params = (('period', 20), ('devfactor', 2))

    def __init__(self):
        self.bollinger = bt.indicators.BollingerBands(
            self.data.close, period=self.params.period, devfactor=self.params.devfactor
        )

        # Initialize lists to store transaction data
        self.transaction_data = []
        self.stock_count = 0
        self.price_avg = 0
        self.starting_cash = self.broker.getcash()
        self.current_month = self.data.datetime.date(0).month

    def next(self):
        cash = self.broker.getcash()
        stock_value = self.broker.getvalue() - cash
        total_cost = self.stock_count * self.price_avg
        today = self.datetime.datetime().strftime('%Y-%m-%d')

        if self.data.datetime.date(0).month != self.current_month:
            self.current_month = self.data.datetime.date(0).month
            cash += add_each_month  # Add $50,000 each month
            self.broker.setcash(cash)

        if self.data.close[0] < self.bollinger.lines.bot[0]:
            shares = (cash * allocation) // self.data.close[0]
            self.stock_count += shares
            self.price_avg = (total_cost + (shares * self.data.close[0])) / self.stock_count
            self.buy(size=shares)
            transaction = [
                today, 'Buy', self.data.close[0], shares,
                self.position.size + shares, stock_value, self.price_avg, cash, self.broker.getvalue()
            ]
            self.transaction_data.append(transaction)

        elif int(self.position.size * 0.5) > 0 and self.data.close[0] > self.bollinger.lines.top[0] \
                and self.data.close[0] > self.price_avg * 1.3:
            sell_shares = int(self.position.size * 0.5)
            self.sell(size=sell_shares)
            transaction = [
                today, 'Sell', self.data.close[0], -1 * sell_shares, self.position.size - sell_shares, stock_value,
                self.price_avg, cash, self.broker.getvalue()
            ]
            self.transaction_data.append(transaction)


if __name__ == '__main__':
    allocation = 0.05
    fee = 0.0035
    years = 5
    add_each_month = 50000
    initial_cash = 1000000
    total_input = initial_cash + add_each_month * years

    start_date = datetime.date.today() - relativedelta(years=years)
    end_date = datetime.date.today()
    stock_id = '2330.TW'

    cerebro = bt.Cerebro()

    data_yf = yf.download(stock_id, start=start_date, end=end_date)
    data = bt.feeds.PandasData(dataname=data_yf)

    cerebro.adddata(data)

    strategy = {
        'Bolling': BollingerBands,
    }

    # Change the strategy to 'Bolling'
    selected_strategy = 'Bolling'
    strategy_class = strategy[selected_strategy]
    cerebro.addstrategy(strategy_class)

    cerebro.broker.setcash(initial_cash)
    cerebro.broker.setcommission(commission=fee)

    cerebro.run()

    # Print buy and sell data in a table
    headers = ['Date', 'Type', 'Price', 'Trade', 'Hold', 'Stock Value', 'Avg. Price', 'Cash', 'Asset']
    strategy_instance = cerebro.runstrats[0][0]
    print('Transaction Data:')
    total_amount = cerebro.broker.getvalue()
    print(f'Input Mount:{total_input} ,Total Amount at the End: {total_amount}')
    print(tabulate(strategy_instance.transaction_data, headers=headers, tablefmt='presto', floatfmt='.1f'))

    # Set the figure size to the maximum screen size
    rcParams['figure.figsize'] = [16, 7.5]  # Adjust the width and height as needed

    # Plotting code
    cerebro.plot(volume=False)
