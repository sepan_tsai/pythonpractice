# Time O(0) Space O(0)
# 1061. Lexicographically Smallest Equivalent String
# https://leetcode.com/problems/lexicographically-smallest-equivalent-string/description/
import collections
import string
import unittest
from typing import List, Optional


class Solution:
    def smallest_equivalent_string1(self, s1: str, s2: str, baseStr: str) -> str:
        neighbors = collections.defaultdict(set)
        for a, b in zip(s1, s2):
            neighbors[a].add(b)
            neighbors[b].add(a)

        memo = {}

        def bfs(ch):
            if ch in memo:
                return memo[ch]
            res = ch
            seen = set()
            queue = {ch}

            while queue:
                c = queue.pop()
                if c in seen:
                    continue
                seen.add(c)
                res = min(res, c)
                queue |= neighbors[c]

            for v in seen:
                memo[v] = res

            return res

        return ''.join(bfs(c) for c in baseStr)

    def smallest_equivalent_string2(self, s1: str, s2: str, baseStr: str) -> str:
        d = {i: i for i in string.ascii_lowercase}

        def find(x):
            if d[x] != x:
                d[x] = find(d[x])
            return d[x]

        def union(x, y):
            print(x, y)
            rx, ry = find(x), find(y)
            print(rx, ry)
            if d[rx] < d[ry]:
                d[ry] = rx
            else:
                d[rx] = ry

        for a, b in zip(s1, s2):
            union(a, b)
            print(s1, s2, d)

        ans = ''
        for s in baseStr:
            ans += find(s)
        return ans


class UnitTest(unittest.TestCase):
    def test_case_1(self):
        sol = Solution()
        s1 = 'parker'
        s2 = 'morris'
        baseStr = 'parser'
        result = sol.smallest_equivalent_string1(s1=s1, s2=s2, baseStr=baseStr)
        self.assertEqual('makkek', result)
        result = sol.smallest_equivalent_string2(s1=s1, s2=s2, baseStr=baseStr)
        self.assertEqual('makkek', result)


if __name__ == '__main__':
    unittest.main()
