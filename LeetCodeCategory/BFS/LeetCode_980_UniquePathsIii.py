# 980. Unique Paths III
# https://leetcode.com/problems/unique-paths-iii/

import unittest
from typing import List, Optional


class Solution:
    @staticmethod
    def unique_paths_iii1(grid: List[List[int]]) -> int:
        l_row, l_col = len(grid), len(grid[0])
        res, flat = 0, sum(grid, [])

        # Time O(3^(M*N)) Space O(M*N)
        def dfs(i, j, left):
            nonlocal res
            grid[i][j] = -1
            for x, y in ((i - 1, j), (i + 1, j), (i, j - 1), (i, j + 1)):
                if -1 < x < l_row and -1 < y < l_col:
                    # if value is not passed
                    if not grid[x][y]:
                        dfs(x, y, left - 1)
                    # if arrived
                    if grid[x][y] == 2:
                        res += not left
            grid[i][j] = 0

        # put first index, and path count should pass
        dfs(*divmod(flat.index(1), l_col), flat.count(0))
        return res


class UnitTest(unittest.TestCase):
    def test_case_1(self):
        input_grid = [[1, 0, 0, 0], [0, 0, 0, 0], [0, 0, 2, -1]]
        sol = Solution()
        result = sol.unique_paths_iii1(grid=input_grid)
        self.assertEqual(2, result)

    def test_case_2(self):
        input_grid = [[0, 1], [2, 0]]
        sol = Solution()
        result = sol.unique_paths_iii1(grid=input_grid)
        self.assertEqual(0, result)


if __name__ == '__main__':
    unittest.main()
