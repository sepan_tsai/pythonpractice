# 797. All Paths From Source to Target
# https://leetcode.com/problems/all-paths-from-source-to-target/

import unittest
from typing import List, Optional


class Solution:

    # Time O(2^N) Space O(N)
    @staticmethod
    def all_paths_source_target1(graph: List[List[int]]) -> List[List[int]]:
        res = []
        end = len(graph) - 1
        queue = [[0, [0]]]

        while queue:
            cur_idx, path = queue.pop(0)
            if cur_idx == end:
                res.append(path)
                continue
            for next_idx in graph[cur_idx]:
                queue.append([next_idx, path + [next_idx]])
        return res

    # Time O(2^N) Space O(N)
    @staticmethod
    def all_paths_source_target2(graph: List[List[int]]) -> List[List[int]]:
        def dfs(cur, path):
            if cur == len(graph) - 1:
                res.append(path)
            else:
                for i in graph[cur]:
                    dfs(i, path + [i])

        res = []
        dfs(0, [0])
        return res


class UnitTest(unittest.TestCase):
    def test_case_1(self):
        input_graph = [[1, 2], [3], [3], []]
        sol = Solution()
        result = sol.all_paths_source_target1(graph=input_graph)
        self.assertEqual([[0, 1, 3], [0, 2, 3]], result)


if __name__ == '__main__':
    unittest.main()
