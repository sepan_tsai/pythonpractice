# 1926. Nearest Exit from Entrance in Maze
# https://leetcode.com/problems/nearest-exit-from-entrance-in-maze/description/

import unittest
from typing import List, Optional


class Solution:
    # Time O(M*N) Space O(M*N))
    @staticmethod
    def nearest_exit1(maze: List[List[str]], entrance: List[int]) -> int:
        visit = [[-1 for dj in range(0, len(maze[0]) + 2)] for _ in range(0, len(maze) + 2)]
        for di in range(0, len(maze)):
            for dj in range(0, len(maze[0])):
                visit[di + 1][dj + 1] = -1 * int(maze[di][dj] == '+')

        i_max = len(maze) - 1
        j_max = len(maze[0]) - 1
        queue = [(entrance[0], entrance[1])]
        move = [[1, 0], [-1, 0], [0, -1], [0, 1]]
        while queue:

            i, j = queue.pop(0)
            step = visit[i + 1][j + 1]

            if not (0 < i < i_max and 0 < j < j_max) and [i, j] != entrance:
                return step

            for di, dj in move:
                if visit[i + di + 1][j + dj + 1] == 0:
                    visit[i + di + 1][j + dj + 1] = step + 1
                    queue.append((i + di, j + dj))
        return -1


class UnitTest(unittest.TestCase):
    def test_case_1(self):
        input_maze = [["+", "+", ".", "+"], [".", ".", ".", "+"], ["+", "+", "+", "."]]
        input_entrance = [1, 2]
        sol = Solution()
        result = sol.nearest_exit1(maze=input_maze, entrance=input_entrance)
        self.assertEqual(1, result)


if __name__ == '__main__':
    unittest.main()
