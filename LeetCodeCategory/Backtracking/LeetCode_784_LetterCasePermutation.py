# 784. Letter Case Permutation
# https://leetcode.com/problems/letter-case-permutation/

import unittest
from typing import List, Optional


class Solution:

    # Time O(N*2^N) Space O(2^N)
    @staticmethod
    def letter_case_permutation1(s: str) -> List[str]:
        n = len(s)
        res = {s}

        for idx in range(n):
            ascIdx = ord(s[idx])

            if 97 <= ascIdx <= 122:
                ascIdx -= 32
            elif 65 <= ascIdx <= 90:
                ascIdx += 32

            for word in list(res):
                res.add(word[:idx] + chr(ascIdx) + word[idx + 1:])

        return sorted(list(res))


class UnitTest(unittest.TestCase):
    def test_case_1(self):
        input_s = "a1b2"
        sol = Solution()
        result = sol.letter_case_permutation1(s=input_s)
        self.assertEqual(sorted(["a1b2", "a1B2", "A1b2", "A1B2"]), result)


if __name__ == '__main__':
    unittest.main()
