# 1415. The k-th Lexicographical String of All Happy Strings of Length N
# https://leetcode.com/problems/the-k-th-lexicographical-string-of-all-happy-strings-of-length-n/

import unittest
from typing import List, Optional


class Solution:
    # Time O(2^N) Space O(2^N)
    @staticmethod
    def get_happy_string1(n: int, k: int) -> str:
        res = ''
        nxt_map = {
            'a': ['b', 'c'],
            'b': ['a', 'c'],
            'c': ['a', 'b'],
        }

        def dfs(st, pos):
            nonlocal res, n, k
            if res:
                return res
            if len(st) == n:
                k -= 1
                if k == 0:
                    res = st
                return res
            for nxt in pos:
                dfs(st + nxt, nxt_map[nxt])

        dfs('a', nxt_map['a'])
        dfs('b', nxt_map['b'])
        dfs('c', nxt_map['c'])
        return res


class UnitTest(unittest.TestCase):
    def test_case_1(self):
        input_n = 3
        input_k = 9
        sol = Solution()
        result = sol.get_happy_string1(n=input_n, k=input_k)
        self.assertEqual('cab', result)


if __name__ == '__main__':
    unittest.main()
