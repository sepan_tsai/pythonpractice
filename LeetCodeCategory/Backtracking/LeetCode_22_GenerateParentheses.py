# 22. Generate Parentheses
# https://leetcode.com/problems/generate-parentheses/

import unittest
from typing import List, Optional


class Solution:

    # Time O(2^N) Space O(2^N)
    @staticmethod
    def generate_parenthesis1(n: int) -> List[str]:
        def generate(string, left, right, count):
            if left > 0:
                generate(string + '(', left - 1, right, count + 1)
            if right > 0 and count > 0:
                generate(string + ')', left, right - 1, count - 1)
            if left == 0 and right == 0:
                res.append(string)

        res = []
        generate('', n, n, 0)
        return res


class UnitTest(unittest.TestCase):
    def test_case_1(self):
        input_n = 3
        sol = Solution()
        result = sorted(sol.generate_parenthesis1(n=input_n))
        self.assertEqual(sorted(["((()))","(()())","(())()","()(())","()()()"]), result)


if __name__ == '__main__':
    unittest.main()
