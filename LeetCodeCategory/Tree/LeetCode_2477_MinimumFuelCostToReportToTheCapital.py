# Time O(0) Space O(0)
# 2477. Minimum Fuel Cost to Report to the Capital
# https://leetcode.com/problems/minimum-fuel-cost-to-report-to-the-capital/description/
import math
import unittest
from collections import defaultdict
from typing import List, Optional


class Tree:
    def __init__(self, val=0):
        self.val = val
        self.children = []


class Solution:
    def __init__(self):
        self.data = None
        self.visit = None

    def minimum_fuel_cost1(self, roads: List[List[int]], seats: int) -> int:
        self.data = {}
        for a, b in roads:
            self.data[a] = self.data.get(a, set()) | {b}
            self.data[b] = self.data.get(b, set()) | {a}

        return self.count_drive(self.build_tree(0, -1), seats)[0]

    def build_tree(self, node, pre):
        parent = Tree(val=node)
        for child in self.data.get(node, []):
            if child == pre:
                continue
            parent.children.append(self.build_tree(child, node))
        return parent

    def count_drive(self, cur, seats):
        drive = 0
        people = 1
        if not cur.children:
            return 1, 1
        for child in cur.children:
            item = self.count_drive(child, seats)
            drive += item[0]
            people += item[1]
        if cur.val == 0:
            return drive, people
        return drive + people // seats + int(people % seats > 0), people

    @staticmethod
    def minimum_fuel_cost2(roads: List[List[int]], seats: int) -> int:
        graph = defaultdict(list)
        for a, b in roads:
            graph[a].append(b)
            graph[b].append(a)

        def dfs(u, p):
            nonlocal res
            cnt = 1
            for v in graph[u]:
                if v == p:
                    continue
                cnt += dfs(v, u)
            if u != 0:
                res += math.ceil(cnt / seats)
            return cnt

        res = 0
        dfs(0, -1)
        return res


class UnitTest(unittest.TestCase):
    def test_case_1(self):
        input_roads = [[3, 1], [3, 2], [1, 0], [0, 4], [0, 5], [4, 6]]
        input_seats = 2
        sol = Solution()
        result = sol.minimum_fuel_cost1(roads=input_roads, seats=input_seats)
        self.assertEqual(7, result)
        result = sol.minimum_fuel_cost2(roads=input_roads, seats=input_seats)
        self.assertEqual(7, result)


if __name__ == '__main__':
    unittest.main()
