# 1008. Construct Binary Search Tree from Preorder Traversal
# https://leetcode.com/problems/construct-binary-search-tree-from-preorder-traversal/

import unittest
from typing import List, Optional


class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


class Solution:
    # Time O(NlogN) Space O(N)
    # Always separate array by root value.
    def bst_from_preorder1(self, preorder: List[int]) -> Optional[TreeNode]:
        if not preorder:
            return None

        node = TreeNode(preorder[0])

        if len(preorder) == 1:
            return node

        left = 1
        right = len(preorder) - 1
        while left <= right:
            mid = (left + right) // 2
            if preorder[mid] < preorder[0]:
                left = mid + 1
            else:
                right = mid - 1

        node.left = self.bst_from_preorder1(preorder[1:left])
        node.right = self.bst_from_preorder1(preorder[left:])

        return node

    # Time O(N) Space O(N)
    # Filled each value to node.
    @staticmethod
    def bst_from_preorder2(preorder: List[int]) -> Optional[TreeNode]:
        def construct(bound):
            if idx[0] == len(preorder) or preorder[idx[0]] > bound:
                return None
            node = TreeNode(preorder[idx[0]])
            idx[0] += 1
            node.left = construct(node.val)
            node.right = construct(bound)

            return node

        idx = [0]
        return construct(float('inf'))


class UnitTest(unittest.TestCase):
    def test_case_1(self):
        input_preorder = [8, 5, 1, 7, 10, 12]
        sol = Solution()
        result = sol.bst_from_preorder1(preorder=input_preorder)
        self.assertEqual([8, 5, 1, 7, 10, 12], get_pre_order(result, []))

    def test_case_2(self):
        input_preorder = []
        sol = Solution()
        result = sol.bst_from_preorder1(preorder=input_preorder)
        self.assertEqual([], get_pre_order(result, []))


def get_pre_order(node, result):
    if node is None:
        return result
    result.append(node.val)
    get_pre_order(node.left, result)
    get_pre_order(node.right, result)
    return result


if __name__ == '__main__':
    unittest.main()
