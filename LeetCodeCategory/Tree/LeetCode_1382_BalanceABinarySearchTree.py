# 1382. Balance a Binary Search Tree
# https://leetcode.com/problems/balance-a-binary-search-tree/

import unittest
from typing import List, Optional


class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


class Solution:
    # Time O(N) Space O(N)
    @staticmethod
    def balance_b_s_t1(root: TreeNode) -> TreeNode:
        def inorder(_node, li):
            if not _node:
                return
            inorder(_node.left, li)
            li.append(_node)
            inorder(_node.right, li)

        def buildBst(_node, li, start, end):
            if not li or start > end:
                return None
            mid = start + (end - start) // 2
            _node = li[mid]
            _node.left = buildBst(_node, li, start, mid - 1)
            _node.right = buildBst(_node, li, mid + 1, end)
            return _node

        if not root:
            return None
        nodes = []
        inorder(root, nodes)
        return buildBst(root, nodes, 0, len(nodes) - 1)


class UnitTest(unittest.TestCase):
    def test_case_1(self):
        input_root = TreeNode(val=1, right=TreeNode(val=2, right=TreeNode(val=3, right=TreeNode(val=4))))
        sol = Solution()
        result = sol.balance_b_s_t1(root=input_root)
        self.assertEqual([2, 1, 3, 4], get_pre_order(result, []))


def get_pre_order(node, result):
    if node is None:
        return result
    result.append(node.val)
    get_pre_order(node.left, result)
    get_pre_order(node.right, result)
    return result


if __name__ == '__main__':
    unittest.main()
