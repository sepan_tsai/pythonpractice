# 222. Count Complete Tree Nodes
# https://leetcode.com/problems/count-complete-tree-nodes/

import unittest
from typing import List, Optional


class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


class Solution:
    # Time O(N) Space O(N)
    def count_nodes1(self, root: Optional[TreeNode], res=0) -> int:
        if root:
            res = 1 + self.count_nodes1(root.left) + self.count_nodes1(root.right)

        return res


class UnitTest(unittest.TestCase):
    def test_case_1(self):
        input_root = TreeNode(val=1,
                              right=TreeNode(val=3, left=TreeNode(val=6)),
                              left=TreeNode(val=2, right=TreeNode(val=5), left=TreeNode(val=4)))
        sol = Solution()
        result = sol.count_nodes1(root=input_root)
        self.assertEqual(6, result)


if __name__ == '__main__':
    unittest.main()
