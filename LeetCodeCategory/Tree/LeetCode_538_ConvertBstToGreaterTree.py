# Time O(0) Space O(0)
# 538. Convert BST to Greater Tree
# https://leetcode.com/problems/convert-bst-to-greater-tree/

import unittest
from typing import List, Optional


class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


class Solution:
    count = 0

    def convert_b_s_t1(self, root: Optional[TreeNode]) -> Optional[TreeNode]:
        if root:
            self.convert_b_s_t1(root.right)
            self.count += root.val
            root.val = self.count
            self.convert_b_s_t1(root.left)

        return root


class UnitTest(unittest.TestCase):
    def test_case_1(self):
        input_root = TreeNode(val=4,
                              right=TreeNode(val=6, right=TreeNode(val=7, right=TreeNode(val=8)), left=TreeNode(val=5)),
                              left=TreeNode(val=1, right=TreeNode(val=2, right=TreeNode(val=3)), left=TreeNode(val=0)))
        sol = Solution()
        result = sol.convert_b_s_t1(root=input_root)
        self.assertEqual([30, 36, 36, 35, 33, 21, 26, 15, 8], get_pre_order(result, []))


def get_pre_order(node, result):
    if node is None:
        return result
    result.append(node.val)
    get_pre_order(node.left, result)
    get_pre_order(node.right, result)
    return result


if __name__ == '__main__':
    unittest.main()
