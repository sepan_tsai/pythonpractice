# 1325. Delete Leaves With a Given Value
# https://leetcode.com/problems/delete-leaves-with-a-given-value/

import unittest
from typing import List, Optional


class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


class Solution:

    # Time O(N) Space O(1)
    # Check left and right is None and val == target
    def remove_leaf_nodes1(self, root: Optional[TreeNode], target: int) -> Optional[TreeNode]:
        if not root:
            return root
        if root.left:
            root.left = self.remove_leaf_nodes1(root.left, target)
        if root.right:
            root.right = self.remove_leaf_nodes1(root.right, target)
        return None if not root.left and not root.right and root.val == target else root


class UnitTest(unittest.TestCase):
    def test_case_1(self):
        input_root = TreeNode(val=1, left=TreeNode(val=2, left=TreeNode(val=2)),
                              right=TreeNode(val=3, left=TreeNode(val=2), right=TreeNode(val=4)))
        input_target = 2
        sol = Solution()
        result = sol.remove_leaf_nodes1(root=input_root, target=input_target)
        self.assertEqual([1, 3, 4], get_pre_order(result, []))


def get_pre_order(node, result):
    if node is None:
        return result
    result.append(node.val)
    get_pre_order(node.left, result)
    get_pre_order(node.right, result)
    return result


if __name__ == '__main__':
    unittest.main()
