# Time O(0) Space O(0)
# 212. Word Search II
# https://leetcode.com/problems/word-search-ii/description/

import unittest
from typing import List, Optional


class TrieNode:
    def __init__(self):
        self.children = {}
        self.isWord = False
        self.refs = 0

    def addWord(self, word):
        cur = self
        cur.refs += 1
        for c in word:
            if c not in cur.children:
                cur.children[c] = TrieNode()
            cur = cur.children[c]
            cur.refs += 1
        cur.isWord = True

    def removeWord(self, word):
        cur = self
        cur.refs -= 1
        for c in word:
            if c in cur.children:
                cur = cur.children[c]
                cur.refs -= 1

    def printChildren(self, word=''):
        cur = self
        for child, childNode in cur.children.items():
            print(cur.refs, child, word + child)
            cur.printChildren(word + child)

class Solution:

    @staticmethod
    def find_words1(board: List[List[str]], words: List[str]) -> List[str]:
        root = TrieNode()
        for w in words:
            root.addWord(w)
        root.printChildren()
        ROWS, COLS = len(board), len(board[0])
        res, visit = set(), set()

        def dfs(_r, _c, node, word):
            if _r < 0 or _c < 0 or _r == ROWS or _c == COLS:
                return None
            if board[_r][_c] not in node.children or node.children[board[_r][_c]].refs < 1 or (_r, _c) in visit:
                return None

            visit.add((_r, _c))
            node = node.children[board[_r][_c]]
            word += board[_r][_c]
            if node.isWord:
                node.isWord = False
                res.add(word)
                root.removeWord(word)

            dfs(_r + 1, _c, node, word)
            dfs(_r - 1, _c, node, word)
            dfs(_r, _c + 1, node, word)
            dfs(_r, _c - 1, node, word)
            visit.remove((_r, _c))

        for r in range(ROWS):
            for c in range(COLS):
                dfs(r, c, root, "")

        return list(res)


class UnitTest(unittest.TestCase):
    def test_case_1(self):
        input_board = [["o","a","a","n"],["e","t","a","e"],["i","h","k","r"],["i","f","l","v"]]
        input_words = ["oath","pea","eat","rain"]
        sol = Solution()
        result = sol.find_words1(board=input_board, words=input_words)
        self.assertEqual(["eat","oath"], result)


if __name__ == '__main__':
    unittest.main()
