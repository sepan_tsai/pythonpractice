# 889. Construct Binary Tree from Preorder and Postorder Traversal
# https://leetcode.com/problems/construct-binary-tree-from-preorder-and-postorder-traversal/

import unittest
from typing import List, Optional


class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


'''
input_preorder = [1, 2, 4, 5, 3, 6, 7]
input_postorder = [4, 5, 2, 6, 7, 3, 1]
Tree
       1
    2     3
   4 5   6 7    
'''


class Solution:
    preIndex, posIndex = 0, 0

    # Time O(N) Space O(N)
    def construct_from_pre_post1(self, preorder: List[int], postorder: List[int]) -> Optional[TreeNode]:
        root = TreeNode(preorder[self.preIndex])
        # preIndex always be a root node
        self.preIndex += 1
        # if not left child, add node to the left. otherwise add it to the right.
        if root.val != postorder[self.posIndex]:
            root.left = self.construct_from_pre_post1(preorder, postorder)
        if root.val != postorder[self.posIndex]:
            root.right = self.construct_from_pre_post1(preorder, postorder)
        self.posIndex += 1
        return root


class UnitTest(unittest.TestCase):
    def test_case_1(self):
        input_preorder = [1, 2, 4, 5, 3, 6, 7]
        input_postorder = [4, 5, 2, 6, 7, 3, 1]
        sol = Solution()
        result = sol.construct_from_pre_post1(preorder=input_preorder, postorder=input_postorder)
        self.assertEqual([1, 2, 4, 5, 3, 6, 7], get_pre_order(result, []))


def get_pre_order(node, result):
    if node is None:
        return result
    result.append(node.val)
    get_pre_order(node.left, result)
    get_pre_order(node.right, result)
    return result

if __name__ == '__main__':
    unittest.main()
