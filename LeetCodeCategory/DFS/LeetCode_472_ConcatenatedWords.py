# Time O(0) Space O(0)
# 472. Concatenated Words
# https://leetcode.com/problems/concatenated-words/description/

import unittest
from typing import List, Optional


class Solution:
    def find_all_concatenated_words_in_a_dict1(self, words: List[str]) -> List[str]:
        d = set(words)

        def dfs(_word):
            for i in range(1, len(_word)):
                prefix = _word[:i]
                suffix = _word[i:]

                if prefix in d and suffix in d:
                    return True
                if prefix in d and dfs(suffix):
                    return True
                if suffix in d and dfs(prefix):
                    return True

            return False

        res = []
        for word in words:
            if dfs(word):
                res.append(word)
        return res

    def find_all_concatenated_words_in_a_dict2(self, words: List[str]) -> List[str]:
        def check(_word, d):
            if _word in d:
                return True

            for i in range(len(_word), 0, -1):
                if _word[:i] in d and check(_word[i:], d):
                    return True
            return False
        res = []
        words_dict = set(words)
        for word in words:
            words_dict.remove(word)
            if check(word, words_dict) is True:
                res.append(word)
            words_dict.add(word)
        return res


class UnitTest(unittest.TestCase):
    def test_case_1(self):
        input_words = ["cat","cats","catsdogcats","dog","dogcatsdog","hippopotamuses","rat","ratcatdogcat"]
        sol = Solution()
        result = sol.find_all_concatenated_words_in_a_dict1(words=input_words)
        self.assertEqual(["catsdogcats","dogcatsdog","ratcatdogcat"], result)
        result = sol.find_all_concatenated_words_in_a_dict2(words=input_words)
        self.assertEqual(["catsdogcats","dogcatsdog","ratcatdogcat"], result)


if __name__ == '__main__':
    unittest.main()
