# 1255. Maximum Score Words Formed by Letters
# https://leetcode.com/problems/maximum-score-words-formed-by-letters/

import unittest
from collections import Counter
from typing import List, Optional


class Solution:

    def __init__(self):
        self.res = 0

    # Time O(N!) Space O(N)
    @staticmethod
    def max_score_words1(words: List[str], letters: List[str], score: List[int]) -> int:
        score = [sum(score[ord(char) - ord('a')] for char in word) for word in words]
        words = list(map(Counter, words))
        res = [0]

        def dfs(i, curr, char):
            res[0] = max(res[0], curr)
            for j, word in enumerate(words[i:], i):
                if not (word - char):
                    dfs(j + 1, curr + score[j], char - word)

        dfs(0, 0, Counter(letters))
        return res[0]


class UnitTest(unittest.TestCase):
    def test_case_1(self):
        input_words = ['add', 'dda', 'bb', 'ba', 'add']
        input_letters = ['a', 'a', 'a', 'a', 'b', 'b', 'b', 'b', 'c', 'c', 'c', 'c', 'c', 'd', 'd', 'd']
        input_score = [3, 9, 8, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        sol = Solution()
        result = sol.max_score_words1(words=input_words, letters=input_letters, score=input_score)
        self.assertEqual(51, result)


if __name__ == '__main__':
    unittest.main()
