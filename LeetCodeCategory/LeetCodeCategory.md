[Reference](https://www.bigocheatsheet.com/)  
![DataStructureOperations.png](DataStructureOperations.png)  
![SortingAlgorithms.png](SortingAlgorithms.png)  

# Array
`M`[18. 4Sum](./Array/LeetCode_18_4Sum.py) `Array` `Two Pointer`    
`H`[354. Russian Doll Envelopes](./Array/LeetCode_354_RussianDollEnvelopes.py) `Array` `Binary Search`    
`M`[654. Maximum Binary Tree](./Array/LeetCode_654_MaximumBinaryTree.py) `Array` `Binary Tree` `Recursion`  
`M`[729. My Calendar I](./Array/LeetCode_729_MyCalendarI.py) `Array` `Binary Search` 
`M`[1850. Minimum Adjacent Swaps to Reach the Kth Smallest Number](./Array/LeetCode_1850_MinimumAdjacentSwapsToReachTheKthSmallestNumber.py) `Array` `Two Pointer` `Backtracking`  
`M`[1996. The Number of Weak Characters in the Game](./Array/LeetCode_1996_TheNumberOfWeakCharactersInTheGame.py) `Array`  
`M`[2125. Number of Laser Beams in a Bank](./Array/LeetCode_2125_NumberOfLaserBeamsInABank.py) `Array`  
`M`[2149. Rearrange Array Elements by Sign](./Array/LeetCode_2149_RearrangeArrayElementsBySign.py) `Array` `Two Pointer`    

# Tree
`M`[1008. Construct Binary Search Tree from Preorder Traversal](./Tree/LeetCode_1008_ConstructBinarySearchTreeFromPreorderTraversal.py) `Array` `Binary Tree` `Recursion`  
`M`[889. Construct Binary Tree from Preorder and Postorder Traversal](./Tree/LeetCode_889_ConstructBinaryTreeFromPreorderAndPostorderTraversal.py) `Array` `Binary Tree`
`M`[1325. Delete Leaves With a Given Value](./Tree/LeetCode_1325_DeleteLeavesWithAGivenValue.py) `Binary Tree` `Recursion`   
`M`[1382. Balance a Binary Search Tree](./Tree/LeetCode_1382_BalanceABinarySearchTree.py) `Binary Tree`  
`M`[538. Convert BST to Greater Tree](./Tree/LeetCode_538_ConvertBstToGreaterTree.py) `Binary Tree`  `DFS` 

# Backtracking  
`M`[22. Generate Parentheses](./Backtracking/LeetCode_22_GenerateParentheses.py) `Backtracking`  
`M`[1415. The k-th Lexicographical String of All Happy Strings of Length N](./Backtracking/LeetCode_1415_TheK_ThLexicographicalStringOfAllHappyStringsOfLengthN.py) `Backtracking`  
`H`[784. Letter Case Permutation](./Backtracking/LeetCode_784_LetterCasePermutation.py) `Backtracking`  

# BFS  
`H`[797. All Paths From Source to Target](./BFS/LeetCode_797_AllPathsFromSourceToTarget.py) `Graph` `BFS`   
`H`[980. Unique Paths III](./BFS/LeetCode_980_UniquePathsIii.py) `Graph` `BFS` `Backtracking`  

# DFS  
`H`[1255. Maximum Score Words Formed by Letters](./DFS/LeetCode_1255_MaximumScoreWordsFormedByLetters.py) `DFS`   

# Dynamic Programing
`M`[62. Unique Paths](./DP/LeetCode_62_UniquePaths.py) `Graph` `Dynamic Programing`    
`M`[63. Unique Paths II](./DP/LeetCode_63_UniquePathsIi.py) `Graph` `Dynamic Programing`    
`M`[474. Ones and Zeroes](./DP/LeetCode_474_OnesAndZeroes.py) `Array` `Dynamic Programing`    
`M`[1277. Count Square Submatrices with All Ones](./DP/LeetCode_1277_CountSquareSubmatricesWithAllOnes.py) `Array` `Dynamic Programing`    
`M`[1638. Count Substrings That Differ by One Character](./DP/LeetCode_1638_CountSubstringsThatDifferByOneCharacter.py) `BFS` `Dynamic Programing` `String`      

# Stack  
`H`[32. Longest Valid Parentheses](./Stack/LeetCode_32_LongestValidParentheses.py) `Stack`

# Sliding Window  
`M`[1004. Max Consecutive Ones III](./SlidingWindow/LeetCode_1004_MaxConsecutiveOnesIii.py) `Array` `Sliding Window`  
`M`[2024. Maximize the Confusion of an Exam](./SlidingWindow/LeetCode_2024_MaximizeTheConfusionOfAnExam.py) `Array` `Sliding Window`  

# Greedy  
`M`[406. Queue Reconstruction by Height](./Greedy/LeetCode_406_QueueReconstructionByHeight.py) `Array` `Greedy`    
`M`[763. Partition Labels](./Greedy/LeetCode_763_PartitionLabels.py) `Array` `Greedy` `Two Pointer`  
`M`[1402. Reducing Dishes](./Greedy/LeetCode_1402_ReducingDishes.py) `Array` `Greedy`  
`M`[1605. Find Valid Matrix Given Row and Column Sums](./Greedy/LeetCode_1605_FindValidMatrixGivenRowAndColumnSums.py) `Array` `Greedy`

# Other
[Iteration and Recursion](./Other/IterationAndRecursion.md) `Iteration` `Recursion`  

# Python
[Binary Search](./Python/Class_BinarySearch.py) `Binary Search`  `bisect`  
[Permutation & Combination](./Python/Class_PermutationAndCombination.py) `Permutation`  `Combination`  `Backtracking`  