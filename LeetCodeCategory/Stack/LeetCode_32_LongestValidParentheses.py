# 32. Longest Valid Parentheses
# https://leetcode.com/problems/longest-valid-parentheses/

import unittest
from typing import List, Optional


class Solution:

    # Time O(N) Space O(N)
    # Record valid start index
    @staticmethod
    def longest_valid_parentheses1(s: str) -> int:
        stack = [-1]
        res = 0
        for idx in range(len(s)):
            if s[idx] == '(':
                stack.append(idx)
            else:
                stack.pop(-1)
                if not stack:
                    stack.append(idx)
                else:
                    res = max(res, idx - stack[-1])
        return res

    # Time O(N) Space O(1)
    # Count ( and ) number, and reset if ) > (
    @staticmethod
    def longest_valid_parentheses2(s: str) -> int:
        res = 0

        l, r = 0, 0
        # traverse the string from left to right
        for i in range(len(s)):
            if s[i] == '(':
                l += 1
            else:
                r += 1
            if l == r:  # valid balanced parentheses substring
                res = max(res, l * 2)
            elif r > l:  # invalid case as ')' is more
                l = r = 0

        l, r = 0, 0
        # traverse the string from right to left
        for i in range(len(s) - 1, -1, -1):
            if s[i] == '(':
                l += 1
            else:
                r += 1
            if l == r:  # valid balanced parentheses substring
                res = max(res, l * 2)
            elif l > r:  # invalid case as '(' is more
                l = r = 0
        return res


class UnitTest(unittest.TestCase):
    def test_case_1(self):
        input_s = '(()'
        sol = Solution()
        result = sol.longest_valid_parentheses1(s=input_s)
        self.assertEqual(2, result)
        result = sol.longest_valid_parentheses2(s=input_s)
        self.assertEqual(2, result)

    def test_case_2(self):
        input_s = ')()())'
        sol = Solution()
        result = sol.longest_valid_parentheses1(s=input_s)
        self.assertEqual(4, result)
        result = sol.longest_valid_parentheses2(s=input_s)
        self.assertEqual(4, result)


if __name__ == '__main__':
    unittest.main()
