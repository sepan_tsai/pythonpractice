# 62. Unique Paths
# https://leetcode.com/problems/unique-paths/

import unittest
from typing import List, Optional


class Solution:
    # Time O(M*N) Space O(M*N)
    @staticmethod
    def unique_paths1(m: int, n: int) -> int:
        dp = [[1 for _ in range(n)] for _ in range(m)]

        for i in range(1, m):
            for j in range(1, n):
                dp[i][j] = dp[i][j - 1] + dp[i - 1][j]

        return dp[-1][-1]

    # Time O(M*N) Space O(N)
    @staticmethod
    def unique_paths2(m: int, n: int) -> int:
        row = [1] * n

        for i in range(m - 1):
            newRow = [1] * n
            for j in range(n - 2, -1, -1):
                newRow[j] = newRow[j + 1] + row[j]
            row = newRow
        return row[0]


class UnitTest(unittest.TestCase):
    def test_case_1(self):
        input_m = 3
        input_n = 7
        sol = Solution()
        result = sol.unique_paths1(m=input_m, n=input_n)
        self.assertEqual(28, result)

    def test_case_2(self):
        input_m = 3
        input_n = 2
        sol = Solution()
        result = sol.unique_paths1(m=input_m, n=input_n)
        self.assertEqual(3, result)


if __name__ == '__main__':
    unittest.main()
