# Time O(0) Space O(0)
# 63. Unique Paths II
# https://leetcode.com/problems/ones-and-zeroes/

import unittest
from typing import List, Optional


class Solution:

    # Time O(M*N) Space O(M*N)
    # Add previous index reach paths
    @staticmethod
    def unique_paths_with_obstacles1(obstacleGrid: List[List[int]]) -> int:
        m = len(obstacleGrid)
        n = len(obstacleGrid[0])

        dp = [[0 for _ in range(0, n)] for _ in range(0, m)]

        for i in range(0, m):
            if obstacleGrid[i][0] == 1:
                break
            dp[i][0] = 1
        for j in range(0, n):
            if obstacleGrid[0][j] == 1:
                break
            dp[0][j] = 1
        for i in range(1, m):
            for j in range(1, n):
                # paths = 0, if it has obstacle
                if obstacleGrid[i][j] == 1:
                    continue
                dp[i][j] = dp[i][j - 1] + dp[i - 1][j]
        return dp[-1][-1]


class UnitTest(unittest.TestCase):
    def test_case_1(self):
        input_obstacleGrid = [[0, 0, 0], [0, 1, 0], [0, 0, 0]]
        sol = Solution()
        result = sol.unique_paths_with_obstacles1(obstacleGrid=input_obstacleGrid)
        self.assertEqual(2, result)

    def test_case_2(self):
        input_obstacleGrid = [[0, 1], [0, 0]]
        sol = Solution()
        result = sol.unique_paths_with_obstacles1(obstacleGrid=input_obstacleGrid)
        self.assertEqual(1, result)


if __name__ == '__main__':
    unittest.main()
