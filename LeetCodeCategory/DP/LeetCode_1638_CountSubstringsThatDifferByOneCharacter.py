# 1638. Count Substrings That Differ by One Character
# https://leetcode.com/problems/count-substrings-that-differ-by-one-character/

import unittest
from typing import List, Optional


class Solution:
    # Time O(N*M) Space O(1)
    @staticmethod
    def count_substrings1(s: str, t: str) -> int:
        n, m = len(s), len(t)

        # move index and get one char diff combination
        # move s 0 index.
        #   aab    => a   aa  get 2 pairs
        #   ab     => b   ab
        # move s 1 index.
        #  aab     =>
        #   ab     =>
        # move s 2 index.
        # aab      => b   get 1 pair
        #   ab     => a
        # move t 1 index.
        #   aab    => a   aa
        #    ab    => b   ab
        # move t 2 index.
        #   aab    => b   get 1 pair
        #     ab   => a
        def helper(i, j):
            res = pre = cur = 0
            for k in range(min(n - i, m - j)):
                print(i, j, k, res, pre, cur)
                cur += 1
                if s[i + k] != t[j + k]:
                    pre, cur = cur, 0
                res += pre
            return res

        return sum(helper(i, 0) for i in range(n)) + sum(helper(0, j) for j in range(1, m))


class UnitTest(unittest.TestCase):
    def test_case_1(self):
        input_s = 'aab'
        input_t = 'ab'
        sol = Solution()
        result = sol.count_substrings1(s=input_s, t=input_t)
        self.assertEqual(4, result)


if __name__ == '__main__':
    unittest.main()
