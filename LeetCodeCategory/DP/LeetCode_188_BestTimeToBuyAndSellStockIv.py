# Time O(0) Space O(0)
# 188. Best Time to Buy and Sell Stock IV
# https://leetcode.com/problems/best-time-to-buy-and-sell-stock-iv/

import unittest
from typing import List, Optional


class Solution:

    @staticmethod
    # Time O(NLogP) Space O(N)
    def max_profit1(k: int, prices: List[int]) -> int:
        n = len(prices)
        if n == 0:
            return 0
        dp = [[0 for i in range(n)] for j in range(k + 1)]
        for i in range(1, k + 1):
            best = -1 * prices[0]
            for j in range(1, n):
                if prices[j] + best < dp[i][j - 1]:
                    dp[i][j] = dp[i][j - 1]
                else:
                    dp[i][j] = prices[j] + best
                if dp[i - 1][j] - prices[j] > best:
                    best = dp[i - 1][j] - prices[j]
        return dp[-1][-1]

    @staticmethod
    # Time O(NK) Space O(NK)
    def max_profit2(k: int, prices: List[int]) -> int:
        n = len(prices)
        if n == 0:
            return 0
        sold = [[0 for _ in range(k + 1)] for _ in range(n)]
        hold = [[-1 * prices[i] for j in range(k + 1)] for i in range(n)]
        for i in range(1, n):
            hold[i][0] = max(hold[i - 1][0], -1 * prices[i])
            for j in range(1, k + 1):
                sold[i][j] = max(sold[i - 1][j], hold[i - 1][j - 1] + prices[i])
                hold[i][j] = max(hold[i - 1][j], sold[i - 1][j] - prices[i])
        return max([sold[-1][j] for j in range(0, k + 1)])


class UnitTest(unittest.TestCase):
    def test_case_1(self):
        input_k = 2
        input_prices = [2, 4, 1]
        sol = Solution()
        result = sol.max_profit1(k=input_k, prices=input_prices)
        self.assertEqual(2, result)
        result = sol.max_profit2(k=input_k, prices=input_prices)
        self.assertEqual(2, result)


if __name__ == '__main__':
    unittest.main()