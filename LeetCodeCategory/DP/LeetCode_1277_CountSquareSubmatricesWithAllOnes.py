# 1277. Count Square Submatrices with All Ones
# https://leetcode.com/problems/count-square-submatrices-with-all-ones/

import unittest
from typing import List, Optional


class Solution:

    # Time O(M*N) Space O(1)
    # matrix[row][col] += min(matrix[row][col - 1], matrix[row - 1][col], matrix[row - 1][col - 1])
    @staticmethod
    def count_squares1(matrix: List[List[int]]) -> int:
        m = len(matrix)
        n = len(matrix[0])
        res = 0

        for row in range(m):
            for col in range(n):
                if matrix[row][col] == 0:
                    continue
                if row == 0 or col == 0:
                    res += 1
                    continue
                matrix[row][col] += min(matrix[row][col - 1], matrix[row - 1][col], matrix[row - 1][col - 1])
                res += matrix[row][col]
        return res


class UnitTest(unittest.TestCase):
    def test_case_1(self):
        input_matrix = [[0, 1, 1, 1], [1, 1, 1, 1], [0, 1, 1, 1]]
        sol = Solution()
        result = sol.count_squares1(matrix=input_matrix)
        self.assertEqual(15, result)

    def test_case_2(self):
        input_matrix = [[1, 0, 1], [1, 1, 0], [1, 1, 0]]
        sol = Solution()
        result = sol.count_squares1(matrix=input_matrix)
        self.assertEqual(7, result)


if __name__ == '__main__':
    unittest.main()
