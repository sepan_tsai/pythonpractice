# 474. Ones and Zeroes
# https://leetcode.com/problems/ones-and-zeroes/

import unittest
from functools import cache
from typing import List, Optional


class Solution:

    # Time O(L*M*N) Space O(L*M*N)
    # Count pick or not pick the string
    @staticmethod
    def find_max_form1(strs: List[str], m: int, n: int) -> int:
        counter = [[s.count("0"), s.count("1")] for s in strs]

        @cache
        def dp(i, j, idx):
            if i < 0 or j < 0:
                return float('-inf')
            if idx == len(strs):
                return 0
            return max(dp(i, j, idx + 1), 1 + dp(i - counter[idx][0], j - counter[idx][1], idx + 1))

        return dp(m, n, 0)

    # Time O(L*M*N) Space O(M*N)
    # Record each index can pick up string dp[i][j] = max(current, 1 + index that can pick up this string)
    @staticmethod
    def find_max_form2(strs: List[str], m: int, n: int) -> int:
        dp = [[0] * (n + 1) for _ in range(m + 1)]
        counter = [[s.count("0"), s.count("1")] for s in strs]

        for zeroes, ones in counter:
            for i in range(m, zeroes - 1, -1):
                for j in range(n, ones - 1, -1):
                    dp[i][j] = max(dp[i][j], 1 + dp[i - zeroes][j - ones])
        return dp[-1][-1]


class UnitTest(unittest.TestCase):
    def test_case_1(self):
        input_strs = ["10", "0001", "111001", "1", "0"]
        input_m = 5
        input_n = 3
        sol = Solution()
        result = sol.find_max_form1(strs=input_strs, m=input_m, n=input_n)
        self.assertEqual(4, result)
        result = sol.find_max_form2(strs=input_strs, m=input_m, n=input_n)
        self.assertEqual(4, result)

    def test_case_2(self):
        input_strs = ["10", "0", "1"]
        input_m = 1
        input_n = 1
        sol = Solution()
        result = sol.find_max_form1(strs=input_strs, m=input_m, n=input_n)
        self.assertEqual(2, result)
        result = sol.find_max_form2(strs=input_strs, m=input_m, n=input_n)
        self.assertEqual(2, result)


if __name__ == '__main__':
    unittest.main()
