# 91. Decode Ways
# https://leetcode.com/problems/decode-ways/

import unittest
from typing import List, Optional


class Solution:
    # Time O(N) Space O(N)
    def num_decodings1(self, s: str) -> int:
        if s[0] == '0':
            return 0
        n = len(s)
        dp = [0] * (n + 2)
        dp[n] = 1
        s += 'x'
        alphabet = [str(i) for i in range(1, 27)]
        for idx in range(n - 1, -1, -1):
            if s[idx] == '0':
                continue

            dp[idx] += dp[idx + 1]
            if (s[idx] + s[idx + 1]) in alphabet:
                dp[idx] += dp[idx + 2]

        return dp[0]


class UnitTest(unittest.TestCase):
    def test_case_1(self):
        input_s = "226"
        sol = Solution()
        result = sol.num_decodings1(s=input_s)
        self.assertEqual(3, result)


if __name__ == '__main__':
    unittest.main()
