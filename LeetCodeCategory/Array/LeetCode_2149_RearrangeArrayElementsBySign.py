# 2149. Rearrange Array Elements by Sign
# https://leetcode.com/problems/rearrange-array-elements-by-sign/

import unittest
from typing import List, Optional


class Solution:
    # Time O(N) Space O(N)
    # Two pointer record index which next number should put in result.
    @staticmethod
    def rearrange_array1(nums: List[int]) -> List[int]:
        length = len(nums)
        res = [0] * length
        positive_idx, negative_idx = 0, 1
        for item in nums:
            if item < 0:
                res[negative_idx] = item
                negative_idx += 2
            else:
                res[positive_idx] = item
                positive_idx += 2
        return res

    # Time O(N) Space O(N)
    # A list record un-used num.
    @staticmethod
    def rearrange_array2(nums: List[int]) -> List[int]:
        tmp_record = []
        res = []
        for num in nums:
            if tmp_record and num >= 0 and tmp_record[0] < 0:
                res.append(num)
                res.append(tmp_record.pop(0))
            elif tmp_record and num < 0 and tmp_record[0] >= 0:
                res.append(tmp_record.pop(0))
                res.append(num)
            else:
                tmp_record.append(num)

        return res


class UnitTest(unittest.TestCase):
    def test_case_1(self):
        input_nums = [3, 1, -2, -5, 2, -4]
        sol = Solution()
        result = sol.rearrange_array1(nums=input_nums)
        self.assertEqual([3, -2, 1, -5, 2, -4], result)
        result = sol.rearrange_array2(nums=input_nums)
        self.assertEqual([3, -2, 1, -5, 2, -4], result)

    def test_case_2(self):
        input_nums = [3, -1]
        sol = Solution()
        result = sol.rearrange_array1(nums=input_nums)
        self.assertEqual([3, -1], result)
        result = sol.rearrange_array2(nums=input_nums)
        self.assertEqual([3, -1], result)


if __name__ == '__main__':
    unittest.main()
