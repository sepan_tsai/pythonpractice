# 654. Maximum Binary Tree
# https://leetcode.com/problems/maximum-binary-tree/

import unittest
from typing import List, Optional


class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


class Solution:
    # Time O(N) Space O(N)
    # Recursion to build each node, always separate nums to left and right until nums is []
    def construct_maximum_binary_tree1(self, nums: List[int]) -> Optional[TreeNode]:
        if len(nums) != 0:
            node = TreeNode()
            node.val = max(nums)
            node.left = self.construct_maximum_binary_tree1(nums[:nums.index(node.val)])
            node.right = self.construct_maximum_binary_tree1(nums[nums.index(node.val) + 1:])
            return node
        else:
            return None


class UnitTest(unittest.TestCase):
    def test_case_1(self):
        input_nums = [3, 2, 1, 6, 0, 5]
        sol = Solution()
        result = sol.construct_maximum_binary_tree1(nums=input_nums)
        result_pre_order = get_pre_order(result, [])
        self.assertEqual([6, 3, 2, 1, 5, 0], result_pre_order)

    def test_case_2(self):
        input_nums = [0]
        sol = Solution()
        result = sol.construct_maximum_binary_tree1(nums=input_nums)
        result_pre_order = get_pre_order(result, [])
        self.assertEqual([0], result_pre_order)


def get_pre_order(node, result):
    if node is None:
        return result
    result.append(node.val)
    get_pre_order(node.left, result)
    get_pre_order(node.right, result)
    return result

if __name__ == '__main__':
    unittest.main()
