# 729. My Calendar I
# https://leetcode.com/problems/my-calendar-i/

import unittest
from typing import List, Optional


class Solution:
    def __init__(self):
        self.books = [[float('-inf'), float('-inf')], [float('inf'), float('inf')]]

    # Time O(logN) Space O(N)
    def book1(self, start: int, end: int) -> bool:
        left = 0
        right = len(self.books) - 1
        while left <= right:
            mid = (left + right) // 2
            if self.books[mid][0] < start:
                left = mid + 1
            elif self.books[mid][0] >= start:
                right = mid - 1
        idx = right

        if start < self.books[idx][1] or end > self.books[idx + 1][0]:
            return False
        self.books.insert(idx + 1, [start, end])
        return True


class UnitTest(unittest.TestCase):
    def test_case_1(self):
        sol = Solution()
        result = sol.book1(start=10, end=20)
        self.assertEqual(True, result)
        result = sol.book1(start=15, end=25)
        self.assertEqual(False, result)
        result = sol.book1(start=20, end=30)
        self.assertEqual(True, result)


if __name__ == '__main__':
    unittest.main()
