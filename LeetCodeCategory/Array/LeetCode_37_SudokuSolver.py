# Time O(0) Space O(0)
# 37. Sudoku Solver
# https://leetcode.com/problems/sudoku-solver/description/

import unittest
from typing import List, Optional


class Solution:
    def __init__(self):
        self.count = 0
        self.res = None

    def solve_sudoku1(self, board: List[List[str]]):
        self.res = False
        self.count = 0
        self.fill_board(board, 0)
        for line in board:
            print(line)
        print(self.count)
        return board

    def fill_board(self, board, idx=0):
        self.count += 1
        if idx >= 81:
            self.res = True
        if self.res:
            return True
        _i = idx // 9
        _j = idx % 9
        tmp = board[_i][_j]
        if tmp != '.':
            self.fill_board(board, idx + 1)
        else:
            for num in self.find_available(board, _i, _j):
                board[_i][_j] = str(num)
                self.fill_board(board, idx + 1)
                if self.res:
                    return True
            else:
                if not self.res:
                    board[_i][_j] = tmp

    @staticmethod
    def find_available(board, i, j):
        available = {'1', '2', '3', '4', '5', '6', '7', '8', '9'}

        _i = i // 3
        _j = j // 3
        for __i in range(0, 3):
            for __j in range(0, 3):
                value = board[_i * 3 + __i][_j * 3 + __j]
                if value in available:
                    available.remove(value)

        available = available - set([board[i][_j] for _j in range(0, 9)])
        available = available - set([board[_i][j] for _i in range(0, 9)])
        return available


class UnitTest(unittest.TestCase):
    def test_case_1(self):
        input_board = [["5", "3", ".", ".", "7", ".", ".", ".", "."],
                       ["6", ".", ".", "1", "9", "5", ".", ".", "."],
                       [".", "9", "8", ".", ".", ".", ".", "6", "."],
                       ["8", ".", ".", ".", "6", ".", ".", ".", "3"],
                       ["4", ".", ".", "8", ".", "3", ".", ".", "1"],
                       ["7", ".", ".", ".", "2", ".", ".", ".", "6"],
                       [".", "6", ".", ".", ".", ".", "2", "8", "."],
                       [".", ".", ".", "4", "1", "9", ".", ".", "5"],
                       [".", ".", ".", ".", "8", ".", ".", "7", "9"]]

        sol = Solution()
        result = sol.solve_sudoku1(board=input_board)
        answer = [['5', '3', '4', '6', '7', '8', '9', '1', '2'],
                  ['6', '7', '2', '1', '9', '5', '3', '4', '8'],
                  ['1', '9', '8', '3', '4', '2', '5', '6', '7'],
                  ['8', '5', '9', '7', '6', '1', '4', '2', '3'],
                  ['4', '2', '6', '8', '5', '3', '7', '9', '1'],
                  ['7', '1', '3', '9', '2', '4', '8', '5', '6'],
                  ['9', '6', '1', '5', '3', '7', '2', '8', '4'],
                  ['2', '8', '7', '4', '1', '9', '6', '3', '5'],
                  ['3', '4', '5', '2', '8', '6', '1', '7', '9']]
        self.assertEqual(answer, result)

        if __name__ == '__main__':
            unittest.main()
