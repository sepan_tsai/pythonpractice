# Time O(0) Space O(0)
# 18. 4Sum
# https://leetcode.com/problems/4sum/description/

import unittest
from typing import List, Optional


class Solution:
    # Time O(N ^ (k - 1)) Space O(N ^ (k - 1))
    @staticmethod
    def four_sum1(nums: List[int], target: int) -> List[List[int]]:
        def find_n_sum(l, r, _target, N, result, _results):
            if r - l + 1 < N or N < 2 or _target < nums[l] * N or _target > nums[r] * N:  # early termination
                return
            if N == 2:  # two pointers solve sorted 2-sum problem
                while l < r:
                    s = nums[l] + nums[r]
                    if s == _target:
                        _results.append(result + [nums[l], nums[r]])
                        l += 1
                        while l < r and nums[l] == nums[l - 1]:
                            l += 1
                    elif s < _target:
                        l += 1
                    else:
                        r -= 1
            else:  # recursively reduce N
                for i in range(l, r + 1):
                    if i == l or (i > l and nums[i - 1] != nums[i]):
                        find_n_sum(i + 1, r, _target - nums[i], N - 1, result + [nums[i]], _results)

        nums.sort()
        results = []
        find_n_sum(0, len(nums) - 1, target, 4, [], results)
        return results


class UnitTest(unittest.TestCase):
    def test_case_1(self):
        input_nums = [1, 0, -1, 0, -2, 2]
        input_target = 0
        sol = Solution()
        result = sol.four_sum1(nums=input_nums, target=input_target)
        self.assertEqual([[-2, -1, 1, 2], [-2, 0, 0, 2], [-1, 0, 0, 1]], result)


if __name__ == '__main__':
    unittest.main()
