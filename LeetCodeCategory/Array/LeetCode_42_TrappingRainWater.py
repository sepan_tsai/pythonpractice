# Time O(0) Space O(0)
# 42. Trapping Rain Water
# https://leetcode.com/problems/trapping-rain-water/

import unittest
from typing import List, Optional


class Solution:

    @staticmethod
    def trap1(height: List[int]) -> int:
        length = len(height)
        l_height = [height[0]] * length
        r_height = [height[-1]] * length

        for i in range(1, length):
            l_height[i] = max(height[i], l_height[i - 1])

        for i in reversed(range(0, length - 1)):
            r_height[i] = max(height[i], r_height[i + 1])

        answer = 0

        for i in range(0, length):
            answer += min(r_height[i], l_height[i]) - height[i]

        return answer


class UnitTest(unittest.TestCase):
    def test_case_1(self):
        input_height = [0, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1]
        sol = Solution()
        result = sol.trap1(height=input_height)
        self.assertEqual(6, result)


if __name__ == '__main__':
    unittest.main()
