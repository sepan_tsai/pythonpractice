# 658. Find K Closest Elements
# https://leetcode.com/problems/find-k-closest-elements/

import unittest
from typing import List, Optional


class Solution:

    # Time O(log(N-K) + K) Space O(1)
    @staticmethod
    def find_closest_elements1(arr: List[int], k: int, x: int) -> List[int]:
        left, right = 0, len(arr) - k
        while left < right:
            mid = (left + right) // 2
            if x - arr[mid] > arr[mid + k] - x:
                left = mid + 1
            else:
                right = mid
        return arr[left:left + k]

    # Time O(N) Space O(N)
    @staticmethod
    def find_closest_elements2(arr: List[int], k: int, x: int) -> List[int]:
        res = []
        for num in arr:
            if len(res) < k:
                res.append(num)
            elif abs(res[0] - x) <= abs(x - num):
                continue
            else:
                res.pop(0)
                res.append(num)
        return res


class UnitTest(unittest.TestCase):
    def test_case_1(self):
        input_arr = [1, 2, 3, 4, 5]
        input_k = 4
        input_x = 3
        sol = Solution()
        result = sol.find_closest_elements1(arr=input_arr, k=input_k, x=input_x)
        self.assertEqual([1, 2, 3, 4], result)
        result = sol.find_closest_elements2(arr=input_arr, k=input_k, x=input_x)
        self.assertEqual([1, 2, 3, 4], result)


if __name__ == '__main__':
    unittest.main()
