# 1850. Minimum Adjacent Swaps to Reach the Kth Smallest Number
# https://leetcode.com/problems/minimum-adjacent-swaps-to-reach-the-kth-smallest-number/

import unittest
from typing import List, Optional


class Solution:

    # Time O(N!) Space O(1)
    @staticmethod
    def get_min_swaps1(num: str, k: int) -> int:
        def nxt_perm(_num: list) -> list:
            nonlocal n
            _i = n - 1
            while _i > 0 and _num[_i - 1] >= _num[_i]:
                _i -= 1
            _j = _i
            while _j < n and _num[_i - 1] < _num[_j]:
                _j += 1
            # Change the last to the first smaller one index from end. ['1', '2', '3'] => ['1', '3', '2']
            _num[_i - 1], _num[_j - 1] = _num[_j - 1], _num[_i - 1]

            # Reversed array after changed index to resort array to smallest ['1', '3', '2'] => ['1', '3', '2']
            _num[_i:] = _num[_i:][::-1]
            return _num

        n = len(num)
        nxt_k_num = list(num)
        # Find the kth bigger
        for _ in range(k):
            nxt_k_num = nxt_perm(nxt_k_num)

        res = 0
        num = list(num)
        for i in range(n):
            j = i
            while j < n and nxt_k_num[i] != num[j]:
                j += 1
            res += j - i
            num[i:j + 1] = [num[j]] + num[i:j]
        return res


class UnitTest(unittest.TestCase):
    def test_case_1(self):
        input_num = "00123"
        input_k = 1
        sol = Solution()
        result = sol.get_min_swaps1(num=input_num, k=input_k)
        self.assertEqual(1, result)


if __name__ == '__main__':
    unittest.main()
