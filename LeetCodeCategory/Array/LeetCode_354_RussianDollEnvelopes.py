# 354. Russian Doll Envelopes
# https://leetcode.com/problems/russian-doll-envelopes/

import unittest
from bisect import bisect_left
from typing import List, Optional


class Solution:
    # Time O(NlogN) Space O(N)
    # Sorted the list, pick up the value insert in result, replace lower one value.
    @staticmethod
    def max_envelopes1(envelopes: List[List[int]]) -> int:
        envelopes.sort(key=lambda envelope: (envelope[0], -envelope[1]))
        res = []
        for i, e in enumerate(envelopes):
            idx = bisect_left(res, e[1])
            if idx == len(res):
                res.append(e[1])
            else:
                res[idx] = e[1]
        return len(res)


class UnitTest(unittest.TestCase):
    def test_case_1(self):
        input_envelopes = [[5, 4], [6, 4], [6, 7], [2, 3]]
        sol = Solution()
        result = sol.max_envelopes1(envelopes=input_envelopes)
        self.assertEqual(3, result)

    def test_case_2(self):
        input_envelopes = [[1,1],[1,1],[1,1]]
        sol = Solution()
        result = sol.max_envelopes1(envelopes=input_envelopes)
        self.assertEqual(1, result)


if __name__ == '__main__':
    unittest.main()
