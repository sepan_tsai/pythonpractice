# 1482. Minimum Number of Days to Make m Bouquets
# https://leetcode.com/problems/minimum-number-of-days-to-make-m-bouquets/

import unittest
from typing import List, Optional


class Solution:

    # Time O(logN) Space O(1)
    def min_days1(self, bloom_day: List[int], m: int, k: int) -> int:
        n = len(bloom_day)
        if m * k > n:
            return -1

        left = 1
        right = max(bloom_day)

        while right > left:
            mid = (left + right) // 2
            enough = self.helper(mid, bloom_day, m, k)
            if enough:
                right = mid
            else:
                left = mid + 1

        return left

    def helper(self, mid, bloom_day, m, k):
        count = 0
        for day in bloom_day:
            if day <= mid:
                count += 1
            else:
                m -= count // k
                count = 0
        else:
            m -= count // k

        return m <= 0


class UnitTest(unittest.TestCase):
    def test_case_1(self):
        input_bloomDay = [1, 10, 3, 10, 2]
        input_m = 3
        input_k = 1
        sol = Solution()
        result = sol.min_days1(bloom_day=input_bloomDay, m=input_m, k=input_k)
        self.assertEqual(3, result)


if __name__ == '__main__':
    unittest.main()
