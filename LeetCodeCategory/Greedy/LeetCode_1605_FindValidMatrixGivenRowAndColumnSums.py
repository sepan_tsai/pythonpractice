# Time O(0) Space O(0)
# 1605. Find Valid Matrix Given Row and Column Sums
# https://leetcode.com/problems/find-valid-matrix-given-row-and-column-sums/

import unittest
from typing import List, Optional


class Solution:

    # Time O(M*N) Space O(M*N)
    @staticmethod
    def restore_matrix1(rowSum: List[int], colSum: List[int]) -> List[List[int]]:
        m = len(rowSum)
        n = len(colSum)
        res = [[0] * n for _ in range(m)]

        i = j = 0
        while i < m and j < n:
            tmp = rowSum[i] if rowSum[i] < colSum[j] else colSum[j]

            res[i][j] = tmp
            rowSum[i] -= tmp
            colSum[j] -= tmp

            if rowSum[i] == 0:
                i += 1
                continue
            j += 1
        return res


class UnitTest(unittest.TestCase):
    def test_case_1(self):
        input_rowSum = [3, 8]
        input_colSum = [4, 7]
        sol = Solution()
        result = sol.restore_matrix1(rowSum=input_rowSum.copy(), colSum=input_colSum.copy())
        result_rowSum = [sum(x) for x in result]
        result_colSum = list(map(sum, zip(*result)))
        self.assertEqual(input_rowSum, result_rowSum)
        self.assertEqual(input_colSum, result_colSum)

    def test_case_2(self):
        input_rowSum = [5, 7, 10]
        input_colSum = [8, 6, 8]
        sol = Solution()
        result = sol.restore_matrix1(rowSum=input_rowSum.copy(), colSum=input_colSum.copy())
        result_rowSum = [sum(x) for x in result]
        result_colSum = list(map(sum, zip(*result)))
        self.assertEqual(input_rowSum, result_rowSum)
        self.assertEqual(input_colSum, result_colSum)


if __name__ == '__main__':
    unittest.main()
