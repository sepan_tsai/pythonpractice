# 763. Partition Labels
# https://leetcode.com/problems/partition-labels/

import unittest
from typing import List, Optional


class Solution:

    # Time O(N) Space O(N)
    @staticmethod
    def partition_labels1(s: str) -> List[int]:
        # last appearance of the letter
        lastIdx = {s[i]: i for i in range(len(s))}
        i, ans = 0, []
        while i < len(s):
            end, j = lastIdx[s[i]], i + 1
            # validation of the part [i, end]
            while j < end:
                if lastIdx[s[j]] > end:
                    # extend the part
                    end = lastIdx[s[j]]
                j += 1

            ans.append(end - i + 1)
            i = end + 1

        return ans


class UnitTest(unittest.TestCase):
    def test_case_1(self):
        input_s = 'eccbbbbdec'
        sol = Solution()
        result = sol.partition_labels1(s=input_s)
        self.assertEqual(8, result)


if __name__ == '__main__':
    unittest.main()
