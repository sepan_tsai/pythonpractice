# 1402. Reducing Dishes
# https://leetcode.com/problems/reducing-dishes/

import unittest
from typing import List, Optional


class Solution:

    # Time O(NlogN) Space O(1)
    # Sorted the list, and greedy pickup.
    @staticmethod
    def max_satisfaction1(satisfaction: List[int]) -> int:
        satisfaction = sorted(satisfaction, reverse=True)
        curFoodSum = 0
        res = 0

        for idx in range(len(satisfaction)):
            curFoodSum += satisfaction[idx]
            if curFoodSum <= 0:
                break
            res += curFoodSum
        return res


class UnitTest(unittest.TestCase):
    def test_case_1(self):
        input_satisfaction = [-1, -8, 0, 5, -9]
        sol = Solution()
        result = sol.max_satisfaction1(satisfaction=input_satisfaction)
        self.assertEqual(14, result)

    def test_case_2(self):
        input_satisfaction = [-1, -4, -5]
        sol = Solution()
        result = sol.max_satisfaction1(satisfaction=input_satisfaction)
        self.assertEqual(0, result)


if __name__ == '__main__':
    unittest.main()
