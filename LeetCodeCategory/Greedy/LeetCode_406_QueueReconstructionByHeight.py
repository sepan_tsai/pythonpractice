# 406. Queue Reconstruction by Height
# https://leetcode.com/problems/queue-reconstruction-by-height/
import heapq
import unittest
from typing import List, Optional


class Solution:

    # Time O(NlogN) Space O(N)
    @staticmethod
    def reconstruct_queue1(people: List[List[int]]) -> List[List[int]]:
        res = []
        heap = []
        for p in people:
            heapq.heappush(heap, [-p[0], p[1]])
        print(list(heap))
        while heap:
            hi = heapq.heappop(heap)
            res.insert(hi[1], [-hi[0], hi[1]])
        return res


class UnitTest(unittest.TestCase):
    def test_case_1(self):
        input_people = [[7, 0], [4, 4], [7, 1], [5, 0], [6, 1], [5, 2]]
        sol = Solution()
        result = sol.reconstruct_queue1(people=input_people)
        self.assertEqual([[5, 0], [7, 0], [5, 2], [6, 1], [4, 4], [7, 1]], result)


if __name__ == '__main__':
    unittest.main()
