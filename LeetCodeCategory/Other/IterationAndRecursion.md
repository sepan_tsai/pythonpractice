# Iteration and Recursion

[Reference](https://afteracademy.com/blog/what-is-the-difference-between-iteration-and-recursion)
![IterationAndRecursion.png](./IterationAndRecursion.png)

### Iteration

```
def factorial(number):  
    res = 1
    for i in range(number):
        res = res * (i + 1)
    return res
``` 

### Recursion

```
def factorial(number):  
    if number <= 1:
        return 1
    else:
        return number * factorial(number - 1)
``` 

- More examples of Iteration and Recursion
    - Traversal of trees: Recursive
    - Dynamic Programming: Both recursive and Iterative
    - Traversal of linear Data Structure: Iterative
    - Depth-First Search: Recursive
    - Breadth-First Search: Iterative
    - Backtracking Algorithms: Recursive
    - Divide-and-Conquer problems: Recursive in nature.
    - Hash Table: Iterative
    - Greedy: Both recursive and Iterative
    - BST: Both Iterative and Recursive
