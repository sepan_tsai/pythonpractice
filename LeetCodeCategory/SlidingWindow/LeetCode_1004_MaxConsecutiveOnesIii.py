# 1004. Max Consecutive Ones III
# https://leetcode.com/problems/max-consecutive-ones-iii/submissions/

import unittest
from typing import List, Optional


class Solution:

    # Time O(N) Space O(1)
    def longest_ones1(self, nums: List[int], k: int) -> int:
        i = 0
        j = 0
        for j in range(len(nums)):

            # add right side
            k -= 1 - nums[j]

            # add left side, if k < 0 window width will keep
            if k < 0:
                k += 1 - nums[i]
                i += 1
        return j - i + 1


class UnitTest(unittest.TestCase):
    def test_case_1(self):
        input_nums = [1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 0]
        input_k = 2
        sol = Solution()
        result = sol.longest_ones1(nums=input_nums, k=input_k)
        self.assertEqual(6, result)


if __name__ == '__main__':
    unittest.main()
