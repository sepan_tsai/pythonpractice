# 2024. Maximize the Confusion of an Exam
# https://leetcode.com/problems/maximize-the-confusion-of-an-exam/

import unittest
from typing import List, Optional


class Solution:
    def maxConsecutiveAnswers(self, answerKey: str, k: int) -> int:
        res = max(self.checkLongSubstring(answerKey, 'T', k), self.checkLongSubstring(answerKey, 'F', k))
        return res

    # Time O(N) Space O(1)
    @staticmethod
    def checkLongSubstring(answerKey, target, allow_diff):
        start_idx = 0
        for idx, key in enumerate(answerKey):
            allow_diff -= int(key != target)
            if allow_diff < 0:
                allow_diff += int(answerKey[start_idx] != target)
                start_idx += 1
        return len(answerKey) - start_idx


class UnitTest(unittest.TestCase):
    def test_case_1(self):
        input_answerKey = "TFFT"
        input_k = 2
        sol = Solution()
        result = sol.maxConsecutiveAnswers(answerKey=input_answerKey, k=input_k)
        self.assertEqual(4, result)


if __name__ == '__main__':
    unittest.main()
