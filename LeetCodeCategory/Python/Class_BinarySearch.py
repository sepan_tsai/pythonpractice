# Binary Search
import unittest
from bisect import bisect_left, bisect_right
from typing import List, Optional


class Solution:
    # Time O(logN) Space O(logN)
    @staticmethod
    def binary_search_left(nums: List[int], num) -> int:
        left = 0
        right = len(nums) - 1
        while left <= right:
            mid = (left + right) // 2
            if nums[mid] < num:
                left = mid + 1
            elif nums[mid] >= num:
                right = mid - 1
        return right

    # Time O(logN) Space O(logN)
    @staticmethod
    def binary_search_right(nums: List[int], num) -> int:
        left = 0
        right = len(nums) - 1
        while left <= right:
            mid = (left + right) // 2
            if nums[mid] <= num:
                left = mid + 1
            elif nums[mid] > num:
                right = mid - 1
        return left


class UnitTest(unittest.TestCase):
    def test_case_1(self):
        input_nums = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
        input_num = 5
        sol = Solution()
        result = sol.binary_search_left(nums=input_nums, num=input_num)
        self.assertEqual(4, result)
        result = bisect_left(input_nums, input_num)
        self.assertEqual(5, result)
        result = bisect_right(input_nums, input_num)
        self.assertEqual(6, result)
        result = sol.binary_search_right(nums=input_nums, num=input_num)
        self.assertEqual(6, result)

    def test_case_2(self):
        input_nums = [1, 1, 3, 3, 5, 5, 7, 7, 9, 9]
        input_num = 5
        sol = Solution()
        result = sol.binary_search_left(nums=input_nums, num=input_num)
        self.assertEqual(3, result)
        result = bisect_left(input_nums, input_num)
        self.assertEqual(4, result)
        result = bisect_right(input_nums, input_num)
        self.assertEqual(6, result)
        result = sol.binary_search_right(nums=input_nums, num=input_num)
        self.assertEqual(6, result)


if __name__ == '__main__':
    unittest.main()
