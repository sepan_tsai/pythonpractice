# Binary Index Tree
# https://yuihuang.com/binary-indexed-tree/?utm_source=rss&utm_medium=rss&utm_campaign=binary-indexed-tree
from typing import List


class BIT:
    def __init__(self, size):
        self.bit = [0] * (size + 1)

    # Time O(log(N)) Space O(N)
    def getSum(self, idx):  # Get sum in range [1..idx], 1-based indexing
        s = 0
        while idx > 0:
            s += self.bit[idx]
            idx -= idx & (-idx)
        return s

    # Time O(log(N)) Space O(N)
    def getSumRange(self, left, right):  # left, right inclusive, 1-based indexing
        return self.getSum(right) - self.getSum(left - 1)

    # Time O(log(N)) Space O(N)
    def addValue(self, idx, val):  # 1-based indexing
        while idx < len(self.bit):
            self.bit[idx] += val
            idx += idx & (-idx)


class Solution:

    def __init__(self, nums: List[int]):
        self.nums = nums
        self.bit = BIT(len(nums))
        for i, v in enumerate(nums):
            self.bit.addValue(i + 1, v)

    def update(self, index: int, val: int) -> None:
        # get diff amount of `val` compared to current value
        diff = val - self.nums[index]

        # add this `diff` amount at index `index+1` of BIT, plus 1 because in BIT it's 1-based indexing
        self.bit.addValue(index + 1, diff)

        # update latest value of `nums[index]`
        self.nums[index] = val

    def sumRange(self, left: int, right: int) -> int:
        return self.bit.getSumRange(left + 1, right + 1)


if __name__ == '__main__':
    sol = Solution(nums=[1, 3, 5])
    print(sol.sumRange(0, 2))
    sol.update(1, 2)
    print(sol.sumRange(0, 2))
