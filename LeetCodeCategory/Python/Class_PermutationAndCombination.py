# Binary Search
import unittest
from bisect import bisect_left, bisect_right
from typing import List, Optional
from itertools import permutations, combinations


class Solution:

    # Time O(N!) Space O(N)
    def backtrack_permutation(self, nums: List[int], result, curr, length):
        if curr >= length - 1:
            result.append(tuple(nums))
        else:
            for idx in range(curr, length):
                nums[curr], nums[idx] = nums[idx], nums[curr]
                self.backtrack_permutation(nums, result, curr + 1, length)
                nums[curr], nums[idx] = nums[idx], nums[curr]  # backtrack
        return result

    @staticmethod
    def itertools_permutation(nums: List[int], pickNumber=-1) -> int:
        if pickNumber < 0:
            return list(permutations(nums))
        return list(permutations(nums, pickNumber))

    @staticmethod
    def itertools_combinations(nums: List[int], pickNumber=1) -> int:
        return list(combinations(nums, pickNumber))


class UnitTest(unittest.TestCase):
    def test_case_1(self):
        input_nums = [1, 2, 3]
        input_pickNumber = 2
        sol = Solution()
        result = sorted(sol.backtrack_permutation(nums=input_nums, result=[], curr=0, length=len(input_nums)))
        self.assertEqual([(1, 2, 3), (1, 3, 2), (2, 1, 3), (2, 3, 1), (3, 1, 2), (3, 2, 1)], result)
        result = sol.itertools_permutation(nums=input_nums)
        self.assertEqual([(1, 2, 3), (1, 3, 2), (2, 1, 3), (2, 3, 1), (3, 1, 2), (3, 2, 1)], result)
        result = sol.itertools_permutation(nums=input_nums, pickNumber=input_pickNumber)
        self.assertEqual([(1, 2), (1, 3), (2, 1), (2, 3), (3, 1), (3, 2)], result)
        result = sol.itertools_combinations(nums=input_nums, pickNumber=input_pickNumber)
        self.assertEqual([(1, 2), (1, 3), (2, 3)], result)


if __name__ == '__main__':
    unittest.main()
