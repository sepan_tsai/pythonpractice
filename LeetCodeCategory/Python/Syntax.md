# Python 

### map()

```
def square(x) :         
    return x ** 2
    
map(square, [1,2,3,4,5])    
<map object at 0x100d3d550>     # return iterator  

list(map(square, [1,2,3,4,5]))   # return list  
[1, 4, 9, 16, 25]

list(map(lambda x: x ** 2, [1, 2, 3, 4, 5]))   # with lambda  
[1, 4, 9, 16, 25]
``` 

### Counter

```
team1 = Counter({'BlackTea': 3, 'GreenTea': 2, 'MilkTea': 1})
team2 = Counter(['BlackTea', 'BlackTea', 'BlackTea', 'MilkTea', 'MilkTea', 'MilkTea'])
team3 = Counter(GreenTea=3, MilkTea=3)

>>> team2['GreanTea'] # Same as dict, team1.get('GreanTea', 0)
0

>>> team1 + team2
Counter({'BlackTea': 6, 'MilkTea': 4, 'GreenTea': 2})

>>> team2 - team3
Counter({'BlackTea': 3})

>>> team2 & team3
Counter({'MilkTea': 3})

>>> team2 | team3
Counter({'MilkTea': 3, 'GreenTea': 3, 'BlackTea': 3})

drink_order = team1 + team2 + team3

>>> drink_order.items()  
dict_items([('MilkTea', 7), ('BlackTea', 6), ('GreenTea', 5)])

>>> drink_order.keys()
dict_keys(['MilkTea', 'BlackTea', 'GreenTea'])

>>> drink_order.values()
dict_values([7, 6, 5])

>>> list(drink_order.elements())
['BlackTea', 'BlackTea', 'BlackTea', 'BlackTea', 'BlackTea', 'BlackTea', 'GreenTea', 'GreenTea', 'GreenTea', 'GreenTea', 'GreenTea', 'MilkTea', 'MilkTea', 'MilkTea', 'MilkTea', 'MilkTea', 'MilkTea', 'MilkTea']

>>> drink_order.most_common(3)
[('MilkTea', 7), ('BlackTea', 6), ('GreenTea', 5)]

>>> drink_order.most_common(2)
[('MilkTea', 7), ('BlackTea', 6)]
``` 
