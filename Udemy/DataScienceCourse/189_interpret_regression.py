import numpy as np
import pandas as pd
import scipy
import statsmodels.api as sm
import matplotlib.pyplot as plt
import seaborn as sns
import sklearn

if __name__ == '__main__':
    sns.set()
    data = pd.read_csv('1.01.SimpleLinearRegression.csv')
    print(data)
    print(data.describe())
    y = data['GPA']
    x1 = data['SAT']

    plt.scatter(x1, y)
    plt.xlabel('SAT', fontsize=20)
    plt.ylabel('GPA', fontsize=20)

    x = sm.add_constant(x1)
    results = sm.OLS(y, x).fit()
    print(results.summary())

    yhat = 0.0017 * x1 + 0.275
    fig = plt.plot(x1, yhat, lw=4, c='orange', label='regression line')
    plt.xlim(0)
    plt.ylim(0)
    plt.show()
