import numpy as np
import pandas as pd
import scipy
import statsmodels.api as sm
import matplotlib.pyplot as plt
import seaborn as sns
import sklearn

if __name__ == '__main__':
    data = pd.read_csv('1.01.SimpleLinearRegression.csv')
    print(data)
    print(data.describe())
    y = data['GPA']
    x1 = data['SAT']

    plt.scatter(x1, y)
    plt.xlabel('SAT', fontsize=20)
    plt.ylabel('GPA', fontsize=20)

    x = sm.add_constant(x1)
    results = sm.OLS(y, x).fit()
    print(results.summary())

    yhat = 0.0017 * x1 + 0.275
    fig = plt.plot(x1, yhat, lw=4, c='orange', label='regression line')
    plt.show()

'''
                            OLS Regression Results                            
==============================================================================
Dep. Variable:                    GPA   R-squared:                       0.406
Model:                            OLS   Adj. R-squared:                  0.399
Method:                 Least Squares   F-statistic:                     56.05
Date:                Sat, 12 Mar 2022   Prob (F-statistic):           7.20e-11
Time:                        16:21:03   Log-Likelihood:                 12.672
No. Observations:                  84   AIC:                            -21.34
Df Residuals:                      82   BIC:                            -16.48
Df Model:                           1                                         
Covariance Type:            nonrobust                                         
==============================================================================
                 coef    std err          t      P>|t|      [0.025      0.975]
------------------------------------------------------------------------------
const          *0.2750      0.409      0.673      0.503      -0.538       1.088
SAT            *0.0017      0.000      7.487      0.000       0.001       0.002
==============================================================================
Omnibus:                       12.839   Durbin-Watson:                   0.950
Prob(Omnibus):                  0.002   Jarque-Bera (JB):               16.155
Skew:                          -0.722   Prob(JB):                     0.000310
Kurtosis:                       4.590   Cond. No.                     3.29e+04
==============================================================================

Notes:
[1] Standard Errors assume that the covariance matrix of the errors is correctly specified.
[2] The condition number is large, 3.29e+04. This might indicate that there are
strong multicollinearity or other numerical problems.
'''