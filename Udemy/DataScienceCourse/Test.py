def yield_test(n):
    print("start n =", n)
    for i in range(n):
        yield i*i
        print("i =", i)

    print("end")


if __name__ == '__main__':
    tests = yield_test(5)
    for test in tests:
        print("test =", test)
        print("--------")