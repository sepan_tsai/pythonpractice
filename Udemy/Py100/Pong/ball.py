import random
from turtle import Turtle


class Ball(Turtle):

    def __init__(self, position=(0, 0)):
        super().__init__()
        self.position = position
        self.color('white')
        self.shape('circle')
        self.penup()
        self.goto(self.position)
        self.x_direction = 0.1
        self.y_direction = 0.1

    def move(self):
        new_x = self.xcor() + self.x_direction
        new_y = self.ycor() + self.y_direction
        self.goto(new_x, new_y)

    def bounce_y(self):
        self.y_direction *= -1

    def bounce_x(self):
        self.x_direction *= -1

    def rest_position(self):
        self.goto(self.position)
        self.bounce_x()
        self.bounce_y()


