from turtle import Screen
from paddle import Paddle
from ball import Ball
from score_board import ScoreBoard

screen = Screen()
screen.bgcolor('black')
screen.setup(width=800, height=600)
screen.title('Pong')
screen.tracer(0)

l_paddle = Paddle((-350, 0))
r_paddle = Paddle((350, 0))
ball = Ball((0, 0))
score_board = ScoreBoard()

screen.listen()

screen.onkey(r_paddle.go_up, 'Up')
screen.onkey(r_paddle.go_down, 'Down')

screen.onkey(l_paddle.go_up, 'w')
screen.onkey(l_paddle.go_down, 's')

game_is_on = True

while game_is_on:
    screen.update()
    ball.move()

    if ball.ycor() > 250 or ball.ycor() < -250:
        ball.bounce_y()

    if 360 > ball.xcor() > 340 and ball.distance(r_paddle) < 50:
        ball.bounce_x()

    if -360 < ball.xcor() < -340 and ball.distance(l_paddle) < 50:
        ball.bounce_x()

    if -380 > ball.xcor():
        ball.rest_position()
        score_board.l_point()

    if 380 < ball.xcor():
        ball.rest_position()
        score_board.r_point()


screen.mainloop()
