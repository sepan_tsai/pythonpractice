import numpy as np

if __name__ == '__main__':
    arr = np.arange(11)
    print(f'{np.sqrt(arr)= }')
    print(f'{np.exp(arr)= }')
    arr1 = np.random.randn(10)
    arr2 = np.random.randn(10)
    print(f'{arr1=}')
    print(f'{arr2=}')
    print(f'{np.add(arr1, arr2)=}')
    print(f'{np.maximum(arr1,arr2)=}')
