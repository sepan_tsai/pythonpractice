from multiprocessing import Pool, cpu_count
from datetime import datetime
from icecream import ic

def stress_test(args):
    """
    :param args:
    :return:
    """
    print(f'args: {args}')
    cpu, value = args
    _start_time = datetime.now()
    for i in range(value):
        value = value * i
    print(f"total: {datetime.now() - _start_time}")


if __name__ == '__main__':

    print(cpu_count())

    start_time = datetime.now()
    cpu_count = cpu_count()
    with Pool(cpu_count) as mp_pool:
        mp_pool.map(stress_test, [(cpu, 10000000) for cpu in range(cpu_count)])
    print(f"Total: {datetime.now() - start_time}")

